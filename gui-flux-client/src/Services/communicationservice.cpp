#include "communicationservice.h"

#include <QVariant>
#include <QVariantMap>
#include <QMetaObject>

#include <iostream>

#include "commonstore.h"
#include "dispatcher.h"

Q_DECLARE_SMART_POINTER_METATYPE(std::shared_ptr)

CommunicationService::CommunicationService(std::shared_ptr<MainRepository> main_repo, QObject *parent)
    : QObject(parent)
    , main_repo_(main_repo)
{

}

void CommunicationService::Start()
{
    std::lock_guard<std::mutex> lock(lock_);
    pub_.StartThread();
    SubscribeBasicTopics();
    sub_.AddCallback(std::bind(&CommunicationService::SubscriberCallback, this, std::placeholders::_1));
    sub_.StartThread();
    std::this_thread::sleep_for(std::chrono::seconds(1));

    QMetaObject::invokeMethod(MainRepository::GetDispatcher(main_repo_).get(),
                              "DispatchAction", Qt::QueuedConnection,
                              Q_ARG(int, Actions::START_LOGIN),
                              Q_ARG(QVariantMap, {}));
}

void CommunicationService::Stop()
{
    std::lock_guard<std::mutex> lock(lock_);
    pub_.StopThread();
    sub_.StopThread();
}

void CommunicationService::HandleAction(const Actions::ACTION_ID & action, const QVariantMap & payload)
{
    switch (action) {
    case Actions::NEW_CHANNEL:
    {
        NewChannel(payload["name"].toString(), payload["description"].toString());
        break;
    }

    case Actions::NEW_USER:
    {
        NewUser(payload["name"].toString(), payload["description"].toString());
        break;
    }

    case Actions::SEND_MESSAGE:
    {
        SendMessage(payload["content"].toString());
        break;
    }

    default:
    {
        break;
    }
    }
}

const ChatState CommunicationService::GetCurrentState() const
{
    return state_;
}

void CommunicationService::NewChannel(QString name, QString description)
{
    std::shared_ptr<chat::Channel> channel(new chat::Channel);
    channel->set_type(chat::Channel_ChannelType_PUBLIC);
    channel->set_name(name.toStdString());
    channel->set_description(description.toStdString());
    AddChannel(channel);
}

void CommunicationService::AddChannel(const std::shared_ptr<chat::Channel> &channel)
{
    std::lock_guard<std::mutex> lock(lock_);
    std::string payload;
    if(channel->SerializeToString(&payload)){
        pub_.Publish(TopicMessage(TOPIC_CHANNEL_ADD, payload));
    }
}


void CommunicationService::NewUser(QString name, QString description)
{
    std::shared_ptr<chat::User> user_proto(new chat::User);
    user_proto->set_name(name.toStdString());
    user_proto->set_description(description.toStdString());
    AddUser(user_proto);
}

void CommunicationService::AddUser(const std::shared_ptr<chat::User> &user)
{
    std::lock_guard<std::mutex> lock(lock_);
    std::string payload;
    if(user->SerializeToString(&payload)){
        pub_.Publish(TopicMessage(TOPIC_USER_ADD, payload));
    }
}

void CommunicationService::SendMessage(QString new_msg)
{
    auto common_model = MainRepository::GetCommonStore(main_repo_);
    std::shared_ptr<chat::Message> msg(new chat::Message);
    msg->set_ms_since_epoch_utc(1); // TODO: fixme
    msg->set_author(common_model->GetCurrentUser()->GetName().toStdString());
    msg->set_content(new_msg.toStdString());
    SendMessage(common_model->GetCurrentChannel()->GetProtoBufObj(), msg);
}

void CommunicationService::SendMessage(const std::shared_ptr<chat::Channel>& channel,
                                          const std::shared_ptr<chat::Message> &message)
{
    std::string payload;
    if(message->SerializeToString(&payload)){
        std::string topic = ParseChannelTopic(*channel);
        pub_.Publish(TopicMessage(topic, payload));
//        ChatCallback(topic, payload);
    }
}

void CommunicationService::FetchServerState()
{
    chat::Request req;
    req.set_id(chat::GET_STATE);
    auto rep = client_.SendRequest(req);
    if (rep && rep->has_chat_state()){
        std::shared_ptr<chat::State> state(new chat::State);
        *state = rep->chat_state();
        state_.ReadChatStateMessage(*state);
        std::cout << "Synced users: " << state_.users_.size() << std::endl;
        std::cout << "Synced channels: " << state_.topic_channel_map_.size() << std::endl;

        QVariantMap payload_map = {{"state", QVariant::fromValue(std::make_shared<ChatStateObject>(state))}};
        QMetaObject::invokeMethod(MainRepository::GetDispatcher(main_repo_).get(),
                                  "DispatchAction", Qt::QueuedConnection,
                                  Q_ARG(int, Actions::UPDATE_CHAT_STATE),
                                  Q_ARG(QVariantMap, payload_map));
    } else {
        std::cerr << "FetchServerState failed!" << std::endl;
        // show error
    }
}

void CommunicationService::SubscribeBasicTopics()
{
    // TODO: select smaller number of topics
    sub_.SubscribeAll();
}

void CommunicationService::SubscriberCallback(const TopicMessage &message)
{
    std::lock_guard<std::mutex> lock(lock_);
    if (message.topic_ == TOPIC_USER_ADD) {
        AddUserCallback(message.payload_);
    } else if (message.topic_ == TOPIC_USER_UPDATE) {
        UpdateUserCallback(message.payload_);
    } else if (message.topic_ == TOPIC_CHANNEL_ADD) {
        AddChannelCallback(message.payload_);
    } else if (message.topic_ == TOPIC_CHANNEL_UPDATE) {
        UpdateChannelCallback(message.payload_);
    } else if (message.topic_.find(TOPIC_CHANNEL_PREFIX) != std::string::npos) {
        ChatCallback(message.topic_, message.payload_);
    }else {
        UnknownCallback(message.topic_, message.payload_);
    }
}

void CommunicationService::AddUserCallback(const std::string &payload)
{
    std::shared_ptr<chat::User> user(new chat::User);
    if (user->ParseFromString(payload)){
        state_.users_.push_back(user);

        QVariantMap payload_map = {{"user", QVariant::fromValue(std::make_shared<UserObject>(user))}};
        QMetaObject::invokeMethod(MainRepository::GetDispatcher(main_repo_).get(),
                                  "DispatchAction", Qt::QueuedConnection,
                                  Q_ARG(int, Actions::ADD_USER),
                                  Q_ARG(QVariantMap, payload_map));
    }
}

void CommunicationService::UpdateUserCallback(const std::string &payload)
{
    chat::User user_update;
    if (user_update.ParseFromString(payload)){
        for(auto & user : state_.users_){
            if (user->name() == user_update.name()){
                user->set_name(user_update.name());
                break;
            }
        }
    }
}

void CommunicationService::AddChannelCallback(const std::string &payload)
{
    std::shared_ptr<chat::Channel> new_channel(new chat::Channel);
    if (new_channel->ParseFromString(payload)){
        state_.topic_channel_map_.insert({ParseChannelTopic(*new_channel), new_channel});

        QVariantMap payload_map = {{"channel", QVariant::fromValue(std::make_shared<ChannelObject>(new_channel))}};
        QMetaObject::invokeMethod(MainRepository::GetDispatcher(main_repo_).get(),
                                  "DispatchAction", Qt::QueuedConnection,
                                  Q_ARG(int, Actions::ADD_CHANNEL),
                                  Q_ARG(QVariantMap, payload_map));
    }
}

void CommunicationService::UpdateChannelCallback(const std::string &payload)
{
    chat::Channel new_channel;
    if (new_channel.ParseFromString(payload)){}
    //TODO
}

void CommunicationService::ChatCallback(const std::string &topic, const std::string &payload)
{
    std::shared_ptr<chat::Message> new_msg(new chat::Message);
    if (new_msg->ParseFromString(payload)){
        auto messages = state_.topic_messages_map_.find(topic);
        if (messages != state_.topic_messages_map_.end()){
            messages->second.push_back(new_msg);
        } else {
            state_.topic_messages_map_.insert({topic, {new_msg}});
        }

        QVariantMap payload_map = {{"message", QVariant::fromValue(std::make_shared<MessageObject>(new_msg))}};
        QMetaObject::invokeMethod(MainRepository::GetDispatcher(main_repo_).get(),
                                  "DispatchAction", Qt::QueuedConnection,
                                  Q_ARG(int, Actions::ADD_MESSAGE),
                                  Q_ARG(QVariantMap, payload_map));
    }
}

void CommunicationService::UnknownCallback(const std::string &topic, const std::string &payload)
{
    std::cerr << "Uknown message from topic " << topic << "\npayload: " << payload << std::endl;
}
