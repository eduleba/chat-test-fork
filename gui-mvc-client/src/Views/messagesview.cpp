#include "messagesview.h"

MessagesView::MessagesView()
    : messages_model_(nullptr)
{
    messages_model_ = std::make_shared<SimpleListModel>();
}

SimpleListModel *MessagesView::GetModel() const
{
    return messages_model_.get();
}

void MessagesView::AddMessageToList(std::shared_ptr<MessageObject> user)
{
    messages_model_->Add(user);
    emit signalModelChanged(GetModel());
}

void MessagesView::SetMessages(std::vector<std::shared_ptr<MessageObject> > msg, bool clean /*= true*/)
{
    if(clean) CleanMessages();

    for(auto & m : msg){
        messages_model_->Add(m);
    }
}

void MessagesView::CleanMessages()
{
    messages_model_->Clean();
}
