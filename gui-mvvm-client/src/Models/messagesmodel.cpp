#include "messagesmodel.h"

#include "communicationservice.h"
#include "commonmodel.h"

MessagesModel::MessagesModel(std::shared_ptr<MainRepository> main_repo, QObject *parent)
    : QObject(parent),
      main_repo_(main_repo)
{
    messages_list_model_ = std::make_shared<SimpleListModel>();

    // Forward processing to main thread
    QObject::connect(MainRepository::GetCommunicationService(main_repo_).get(), &CommunicationService::signalNewMessage,
                     this, &MessagesModel::NewMessageUpdate, Qt::QueuedConnection);

    QObject::connect(MainRepository::GetCommunicationService(main_repo_).get(), &CommunicationService::signalNewServerState,
                     this, &MessagesModel::ChatStateUpdate, Qt::QueuedConnection);

}

SimpleListModel *MessagesModel::GetListModel()
{
    return messages_list_model_.get();
}

void MessagesModel::AddMessageToList(std::shared_ptr<MessageObject> user)
{
    messages_list_model_->Add(user);
    emit signalMessagesListChanged();
}

void MessagesModel::SetMessages(std::vector<std::shared_ptr<MessageObject> > msg, bool clean /*= true*/)
{
    if(clean) CleanMessages();

    for(auto & m : msg){
        messages_list_model_->Add(m);
    }
}

void MessagesModel::CleanMessages()
{
    messages_list_model_->Clean();
}

void MessagesModel::ShowCurrentChannelMessages()
{
    auto & state = MainRepository::GetCommunicationService(main_repo_)->GetCurrentState();
    auto channel = state.topic_messages_map_.find(ParseChannelTopic(*MainRepository::GetCommonModel(main_repo_)->GetCurrentChannel()->GetProtoBufObj()));
    if(channel != state.topic_messages_map_.end() && !channel->second.empty()){
        std::vector <std::shared_ptr<MessageObject>> messages;
        for(auto & msg : channel->second){
            messages.push_back(std::make_shared<MessageObject>(msg));
        }
        SetMessages(messages, true);
    } else {
        CleanMessages();
    }
}

void MessagesModel::ChatStateUpdate(const std::shared_ptr<ChatStateObject> &state)
{
    // TODO: use channels messages
}

void MessagesModel::NewMessageUpdate(const std::shared_ptr<MessageObject> &message)
{
    AddMessageToList(message);
}

void MessagesModel::SendMessage(QString new_msg)
{
    auto common_model = MainRepository::GetCommonModel(main_repo_);
    std::shared_ptr<chat::Message> msg(new chat::Message);
    msg->set_ms_since_epoch_utc(1); // TODO: fixme
    msg->set_author(common_model->GetCurrentUser()->GetName().toStdString());
    msg->set_content(new_msg.toStdString());
    MainRepository::GetCommunicationService(main_repo_)->SendMessage(common_model->GetCurrentChannel()->GetProtoBufObj(), msg);
}

void MessagesModel::SendMessageObj(std::shared_ptr<MessageObject> &new_msg)
{
    MainRepository::GetCommunicationService(main_repo_)->SendMessage(MainRepository::GetCommonModel(main_repo_)->GetCurrentChannel()->GetProtoBufObj(), new_msg->GetProtoBufObj());
}
