#include "usersstore.h"

#include <iostream>

#include "communicationservice.h"
#include "commonstore.h"

Q_DECLARE_SMART_POINTER_METATYPE(std::shared_ptr)

UsersStore::UsersStore(std::shared_ptr<MainRepository> main_repo, QObject *parent)
    : QObject(parent),
      main_repo_(main_repo)
{
    users_list_model_ = std::make_shared<SimpleListModel>();
}

void UsersStore::HandleAction(const Actions::ACTION_ID &action, const QVariantMap &payload)
{
    if (action == Actions::UPDATE_CHAT_STATE) {
        if (payload.contains("state") && payload["state"].canConvert<std::shared_ptr<ChatStateObject>>()) {
            ChatStateUpdate(payload["state"].value<std::shared_ptr<ChatStateObject>>());
        }
    } else if (action == Actions::ADD_USER) {
        if (payload.contains("user") && payload["user"].canConvert<std::shared_ptr<UserObject>>()) {
            NewUserUpdate(payload["user"].value<std::shared_ptr<UserObject>>());
        }
    }
}

SimpleListModel *UsersStore::GetListModel()
{
    return users_list_model_.get();
}

void UsersStore::AddUserToList(std::shared_ptr<UserObject> user)
{
    users_list_model_->Add(user);
    emit signalUsersListChanged();
}

void UsersStore::ChatStateUpdate(const std::shared_ptr<ChatStateObject> &state)
{
    for(auto & user : state->GetUsers()){
        NewUserUpdate(user);
    }
}

void UsersStore::NewUserUpdate(const std::shared_ptr<UserObject> &user)
{
    AddUserToList(user);
}
