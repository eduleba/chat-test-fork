#pragma once

#include <string>

#include "proto/chat.pb.h"

struct TopicMessage
{
    TopicMessage(std::string topic, std::string payload)
        : topic_(topic), payload_(payload) {}
    std::string topic_;
    std::string payload_;
};

namespace {
  // Connections
  const std::string IPC_SOCKET_PUB = "ipc://test.ipc";
  const std::string IPC_SOCKET_SUB = "ipc://test.ipc";
  const std::string TCP_SOCKET_PUB = "tcp://127.0.0.1:5060";
  const std::string TCP_SOCKET_SUB = "tcp://127.0.0.1:5059";
  const std::string TCP_SOCKET_REQ = "tcp://127.0.0.1:5061";
  const std::string TCP_SOCKET_REP = "tcp://127.0.0.1:5062";
  const std::string TCP_SOCKET = "tcp://127.0.0.1:5000";

  //Topics
  const std::string TOPIC_STATUS = "/status";

  const std::string TOPIC_USER_ADD = "/user/add";
  const std::string TOPIC_USER_UPDATE = "/user/update";
//  const std::string TOPIC_USER_LIST = "/user/list";

  const std::string TOPIC_CHANNEL_ADD = "/channel/add";
  const std::string TOPIC_CHANNEL_UPDATE = "/channel/update";
//  const std::string CHANNEL_LIST = "/channel/list";

  const std::string TOPIC_CHANNEL_PREFIX = "/channel/";
  const std::string TOPIC_CHANNEL_CHAT = TOPIC_CHANNEL_PREFIX + "{0}";
  const std::string TOPIC_CHANNEL_DIRECT_CHAT = TOPIC_CHANNEL_PREFIX + "{0}/{1}";
}

std::string ParseChannelTopic(const chat::Channel & channel);
