#pragma once

#include <atomic>
#include <functional>
#include <thread>

#include <zmq.hpp>

#include "common.h"
#include "proto/chat.pb.h"

class ServerSocket
{
public:
    ServerSocket(bool bind = false);
    ServerSocket(const std::string& address, bool bind = false);
    ~ServerSocket();

    void StartThread();
    void AddCallback(std::function<chat::Response(const chat::Request&)> callback_);
    void StopThread();

private:
    std::string CallbackWrapper(const std::string& request);
    void SetupZMQ();
    void Stop();
    void ServerLoop();

    bool bind_socket_;
    std::string address_;
    std::unique_ptr<zmq::context_t> context_;
    std::unique_ptr<zmq::socket_t> zmq_server_;
    std::atomic<bool> loop_running_;
    std::thread thread_;
    std::function<chat::Response(const chat::Request&)> callback_;
};
