#include "messagesstore.h"

#include "communicationservice.h"
#include "commonstore.h"

Q_DECLARE_SMART_POINTER_METATYPE(std::shared_ptr)

MessagesStore::MessagesStore(std::shared_ptr<MainRepository> main_repo, QObject *parent)
    : QObject(parent),
      main_repo_(main_repo)
{
    messages_list_model_ = std::make_shared<SimpleListModel>();
}

void MessagesStore::HandleAction(const Actions::ACTION_ID &action, const QVariantMap &payload)
{
    if (action == Actions::UPDATE_CHAT_STATE){
        if (payload.contains("state") && payload["state"].canConvert<std::shared_ptr<ChatStateObject>>()) {
            ChatStateUpdate(payload["state"].value<std::shared_ptr<ChatStateObject>>());
        }
    } else if (action == Actions::ADD_MESSAGE) {
        if (payload.contains("message") && payload["message"].canConvert<std::shared_ptr<MessageObject>>()) {
            NewMessageUpdate(payload["message"].value<std::shared_ptr<MessageObject>>());
        }
    }
}

SimpleListModel *MessagesStore::GetListModel()
{
    return messages_list_model_.get();
}

void MessagesStore::AddMessageToList(std::shared_ptr<MessageObject> user)
{
    messages_list_model_->Add(user);
    emit signalMessagesListChanged();
}

void MessagesStore::SetMessages(std::vector<std::shared_ptr<MessageObject> > msg, bool clean /*= true*/)
{
    if(clean) CleanMessages();

    for(auto & m : msg){
        messages_list_model_->Add(m);
    }
}

void MessagesStore::CleanMessages()
{
    messages_list_model_->Clean();
}

void MessagesStore::ShowCurrentChannelMessages()
{
    auto & state = MainRepository::GetCommunicationService(main_repo_)->GetCurrentState();
    auto channel = state.topic_messages_map_.find(ParseChannelTopic(*MainRepository::GetCommonStore(main_repo_)->GetCurrentChannel()->GetProtoBufObj()));
    if(channel != state.topic_messages_map_.end() && !channel->second.empty()){
        std::vector <std::shared_ptr<MessageObject>> messages;
        for(auto & msg : channel->second){
            messages.push_back(std::make_shared<MessageObject>(msg));
        }
        SetMessages(messages, true);
    } else {
        CleanMessages();
    }
}

void MessagesStore::ChatStateUpdate(const std::shared_ptr<ChatStateObject> &state)
{
    // TODO: use channels messages
}

void MessagesStore::NewMessageUpdate(const std::shared_ptr<MessageObject> &message)
{
    AddMessageToList(message);
}
