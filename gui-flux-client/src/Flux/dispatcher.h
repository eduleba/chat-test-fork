#pragma once

#include <functional>
#include <memory>
#include <vector>

#include <QObject>
#include <QVariantMap>

#include "action.h"

class Dispatcher : public QObject
{
    Q_OBJECT
public:
    explicit Dispatcher(QObject *parent = nullptr);

    void AddCallback(std::function<void(const Actions::ACTION_ID&, const QVariantMap&)> callback);

    template<typename T>
    void AddCallback(QObject* obj){
        AddCallback(std::bind(&T::HandleAction, obj, std::placeholders::_1, std::placeholders::_2));
    }

signals:

public slots:
    void DispatchStartAppAction();
    Q_INVOKABLE void DispatchAction(int id);
    Q_INVOKABLE void DispatchAction(int id, QVariantMap payload);

private:
    std::vector<std::function<void(const Actions::ACTION_ID&, const QVariantMap&)>> callbacks_;
};
