#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickStyle>
#include <QQmlContext>
#include <QTimer>

#include "Flux/dispatcher.h"
#include "Flux/action.h"

#include "Services/communicationservice.h"
#include "Services/viewshelperservice.h"
#include "Services/mainrepository.h"

#include "Stores/channelsstore.h"
#include "Stores/commonstore.h"
#include "Stores/messagesstore.h"
#include "Stores/usersstore.h"

Q_DECLARE_SMART_POINTER_METATYPE(std::shared_ptr)

void RegisterComponents(){

    qRegisterMetaType<std::shared_ptr<MessageObject>>("std::shared_ptr<MessageObject>");
    qRegisterMetaType<std::shared_ptr<UserObject>>("std::shared_ptr<UserObject>");
    qRegisterMetaType<std::shared_ptr<ChannelObject>>("std::shared_ptr<ChannelObject>");
    qRegisterMetaType<std::shared_ptr<ChatStateObject>>("std::shared_ptr<ChatStateObject>");

    qmlRegisterUncreatableMetaObject(
      Actions::staticMetaObject, // static meta object
      "Flux",                // import statement (can be any string)
      1, 0,                          // major and minor version of the import
      "Actions",                 // name in QML (does not have to match C++ name)
      "Error: only enums"            // error in case someone tries to create a MyNamespace object
    );
}

int main(int argc, char *argv[])
{
    QQuickStyle::setStyle("Material");
    QCoreApplication::addLibraryPath("./");

    QGuiApplication app(argc, argv);

    RegisterComponents();

    auto main_repo = std::make_shared<MainRepository>();
    auto dispatcher = std::make_shared<Dispatcher>();

    main_repo->SetDispatcher(dispatcher);

    auto views_helper = std::make_shared<ViewsHelper>();
    auto comm_service = std::make_shared<CommunicationService>(main_repo);

    main_repo->SetServices(views_helper,
                           comm_service);

    dispatcher->AddCallback(std::bind(&ViewsHelper::HandleAction,
                                      views_helper.get(),
                                      std::placeholders::_1,
                                      std::placeholders::_2));

    dispatcher->AddCallback(std::bind(&CommunicationService::HandleAction,
                                      comm_service.get(),
                                      std::placeholders::_1,
                                      std::placeholders::_2));

    auto users_store = std::make_shared<UsersStore>(main_repo);
    auto channels_store = std::make_shared<ChannelsStore>(main_repo);
    auto messages_store = std::make_shared<MessagesStore>(main_repo);
    auto common_store = std::make_shared<CommonStore>(main_repo);

    main_repo->SetStores(channels_store,
                         common_store,
                         messages_store,
                         users_store);

    dispatcher->AddCallback(std::bind(&CommonStore::HandleAction,
                                      common_store.get(),
                                      std::placeholders::_1,
                                      std::placeholders::_2));

    dispatcher->AddCallback(std::bind(&UsersStore::HandleAction,
                                      users_store.get(),
                                      std::placeholders::_1,
                                      std::placeholders::_2));

    dispatcher->AddCallback(std::bind(&ChannelsStore::HandleAction,
                                      channels_store.get(),
                                      std::placeholders::_1,
                                      std::placeholders::_2));

    dispatcher->AddCallback(std::bind(&MessagesStore::HandleAction,
                                      messages_store.get(),
                                      std::placeholders::_1,
                                      std::placeholders::_2));

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty(
                     "UsersStore",
                     users_store.get());

    engine.rootContext()->setContextProperty(
                     "ChannelsStore",
                     channels_store.get());

    engine.rootContext()->setContextProperty(
                     "MessagesStore",
                     messages_store.get());

    engine.rootContext()->setContextProperty(
                     "CommonStore",
                     common_store.get());

    engine.rootContext()->setContextProperty(
                     "ViewsHelper",
                     views_helper.get());

    engine.rootContext()->setContextProperty(
                     "Dispatcher",
                     dispatcher.get());

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    QTimer::singleShot(0, dispatcher.get(), &Dispatcher::DispatchStartAppAction);

    return app.exec();
}

