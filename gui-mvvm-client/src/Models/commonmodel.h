#pragma once

#include <future>
#include <memory>

#include <QObject>

#include "mainrepository.h"

#include "channelobject.h"
#include "userobject.h"

class CommonModel : public QObject
{
    Q_OBJECT
public:
    explicit CommonModel(std::shared_ptr<MainRepository> main_repo, QObject *parent = nullptr);

    std::shared_ptr<ChannelObject> GetCurrentChannel();
    std::shared_ptr<UserObject> GetCurrentUser();

signals:

public slots:
    void CurrentUserUpdate(std::shared_ptr<UserObject> & user);
    void SetUser(std::shared_ptr<UserObject>& user);

    void CurrentChannelUpdate(std::shared_ptr<ChannelObject> & channel);
    void SetChannel(std::shared_ptr<ChannelObject>& channel);

    void ShowLoginPopup();
    void LoginUser(std::shared_ptr<UserObject>& user);

    void SelectChannel(QString name);

    void StartApp();

private:
    std::weak_ptr<MainRepository> main_repo_;
    std::shared_ptr<ChannelObject> current_channel_;
    std::shared_ptr<UserObject> current_user_;
    std::future<void> communication_start_;
};
