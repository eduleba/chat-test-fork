import QtQuick 2.3
import QtQuick.Controls.Material 2.2
import QtQuick.Controls 2.2

import Flux 1.0

Item {
    id: idMessagesView
    clip: true

    Flickable {
        id: idFlickable
        anchors.fill: idMessagesView
        contentHeight: idContentCol.height
        Column {
            id: idContentCol
            spacing: 5
            Repeater {
                model: MessagesStore.messages_list_model
                Pane {

                    //                anchors.centerIn: parent
                    width: 120
                    height: 60
                    anchors.margins: 5

                    Material.elevation: idMouseArea.containsMouse ? 3 : 6
                    Material.background: Material.Amber

                    MouseArea {
                        id: idMouseArea
                        anchors.fill: parent
                        hoverEnabled: true
                    }

                    Label {
                        text: model.author + ": " + model.content
                        anchors.centerIn: parent
                    }
                }
            }
        }
    }
}
