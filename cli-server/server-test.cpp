#include <iostream>
#include <sstream>
#include <string>
#include "proto/chat.pb.h"

#include "subscriber.h"

using namespace std;

int main (int argc, char *argv[])
{
    GOOGLE_PROTOBUF_VERIFY_VERSION;

    Subscriber sub(true);
    sub.SubscribeTopic("test");
    sub.StartThread();

    std::string msg;
    std::cin >> msg;

    sub.StopThread();

    google::protobuf::ShutdownProtobufLibrary();
    return 0;
}
