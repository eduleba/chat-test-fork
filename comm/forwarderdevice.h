#pragma once

#include <zmq.hpp>

class ForwarderDevice
{
public:
    ForwarderDevice();

    void Run();
private:
    zmq::context_t context_;
    zmq::socket_t zmq_publisher_;
    zmq::socket_t zmq_subscriber_;
};

