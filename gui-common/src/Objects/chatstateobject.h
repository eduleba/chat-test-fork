#pragma once

#include <memory>

#include <QObject>
#include <QString>

#include "proto/chat.pb.h"

#include "channelobject.h"
#include "userobject.h"
#include "messageobject.h"

class ChatStateObject : public QObject
{
    Q_OBJECT
public:
    explicit ChatStateObject(std::shared_ptr<chat::State> state, QObject *parent = nullptr);

    std::vector<std::shared_ptr<UserObject>> GetUsers() const;
    std::vector<std::shared_ptr<ChannelObject>> GetChannels() const;
    std::vector<std::shared_ptr<MessageObject>> GetChannelMessages(QString channel_name) const;
    std::shared_ptr<chat::State> GetProtoBufObj() const;

signals:

public slots:

private:
    std::shared_ptr<chat::State> state_;
};
