#include <iostream>

#include "forwarderdevice.h"

using namespace std;

int main ()
{

    // extract Forwarder to another process
    ForwarderDevice fwd;
    std::cerr << "Forwarder started." << std::endl;
    fwd.Run();
    std::cerr << "Forwarder stoped." << std::endl;

    return 0;
}
