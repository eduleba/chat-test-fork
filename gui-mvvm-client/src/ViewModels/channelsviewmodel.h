#pragma once

#include <memory>

#include <QObject>

#include "simplelistmodel.h"
#include "channelsmodel.h"

class ChannelsViewModel : public QObject
{
    Q_OBJECT
    Q_PROPERTY(SimpleListModel* channels_list_model READ GetModel NOTIFY signalModelChanged)
    Q_PROPERTY(ChannelObject* current_channel READ GetCurrentChannel NOTIFY signalChannelChanged)
public:
    explicit ChannelsViewModel(std::shared_ptr<ChannelsModel> channels_model, QObject *parent = nullptr);

    SimpleListModel *GetModel() const;
    ChannelObject *GetCurrentChannel() const;

    Q_INVOKABLE void SelectChannel(QString channel_name);
    Q_INVOKABLE void NewChannel(QString name, QString description);
    Q_INVOKABLE void AddChannel();
    Q_INVOKABLE void RemoveChannel(QString name);

signals:
    void signalModelChanged(SimpleListModel* users_model);
    void signalChannelChanged(ChannelObject* current_user);
    void signalAddChannel();

private:
    std::shared_ptr<ChannelsModel> model_;
};
