#pragma once

#include <QObject>

namespace Actions
{
    Q_NAMESPACE         // required for meta object creation
    enum ACTION_ID {
    // General 0-100
        START_APP = 0,
        START_LOGIN = 1,
        UPDATE_CHAT_STATE = 2,
    // Users 100-200
        LOGIN_USER = 100,
        ADD_USER = 101,
        NEW_USER = 102,
    // Channels 200-300
        SELECT_CHANNEL = 200,
        ADD_CHANNEL = 201,
        NEW_CHANNEL = 202,
        REMOVE_CHANNEL = 203,
    // Messamges 300-400
        SEND_MESSAGE = 300,
        ADD_MESSAGE = 301,
    // Views 400-500
        SHOW_BUSY = 400,
        HIDE_BUSY = 401,
        SHOW_LOGIN = 402,
        HIDE_LOGIN = 403,
        OPEN_CHANNEL_POPUP = 404,
        OPEN_USER_POPUP = 405
    };
    Q_ENUM_NS(ACTION_ID)  // register the enum in meta object data
}

class Action : public QObject
{
    Q_OBJECT
public:
    explicit Action(QObject *parent = nullptr);

signals:

public slots:
};
