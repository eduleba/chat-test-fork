#include "publisher.h"

#include <iostream>

Publisher::Publisher(bool bind /*= false*/)
 : bind_socket_(bind), address_(TCP_SOCKET_PUB), loop_running_(false)
{
    SetupZMQ();
}

Publisher::Publisher(const std::string &address, bool bind /*= false*/)
 : bind_socket_(bind), address_(address), loop_running_(false)
{
    SetupZMQ();
}

Publisher::~Publisher()
{
    StopThread();
}

void Publisher::StartThread()
{
    if (loop_running_) return;
    if (context_ == nullptr){
        SetupZMQ();
    }
    thread_ = std::thread(&Publisher::PubLoop, this);
    loop_running_ = true;
    std::this_thread::sleep_for(std::chrono::seconds(1));
}

void Publisher::Publish(const TopicMessage &topic_message)
{
    {
        std::lock_guard<std::mutex> lock(lock_);
        queue_.push(topic_message);
    }
    condition_var_.notify_one();
}

void Publisher::StopThread()
{
    if (!loop_running_) return;
    loop_running_ = false;
    PubStopMessage();
    if (thread_.joinable()){
        thread_.join();
    }
    zmq_publisher_.reset(nullptr);
    context_.reset(nullptr);
}

void Publisher::SetupZMQ()
{
    context_.reset(new zmq::context_t(1));
    zmq_publisher_.reset(new zmq::socket_t(*context_, ZMQ_PUB));
    if (bind_socket_){
        zmq_publisher_->bind(address_);
    } else {
        zmq_publisher_->connect(address_);
    }
}

void Publisher::PubStopMessage()
{
    {
        std::lock_guard<std::mutex> lock(lock_);
        while (!queue_.empty()) {
            queue_.pop();
        }
        queue_.push(TopicMessage("", ""));
    }
    condition_var_.notify_one();
}

void Publisher::PubLoop()
{
    while(true){
        std::unique_lock<std::mutex> lock(lock_);
        condition_var_.wait(lock, [this](){return !queue_.empty();});

        auto & msg = queue_.front();

        // Empty message breaks loop
        if (msg.topic_.empty() && msg.payload_.empty()) break;

        size_t size = msg.topic_.size();
        auto topic_size = zmq_publisher_->send(msg.topic_.c_str(), size, ZMQ_SNDMORE);
        if (size != topic_size){
            // log issue
            std::cerr << "E1" << std::endl;
            queue_.pop();
            continue;
        }
        else {
//            std::cerr << "Published to: " << msg.topic_ << std::endl;
        }

        size = msg.payload_.size();
        auto payload_size = zmq_publisher_->send(msg.payload_.c_str(), size);
        if (size != payload_size){
            // log issue
            std::cerr << "E1" << std::endl;
            queue_.pop();
            continue;
        }
        else {
//            std::cerr << "Payload: " << msg.payload_ << std::endl;
        }
        queue_.pop();
    }
}
