#include "channeledit.h"

ChannelEdit::ChannelEdit()
    : channel_(nullptr)
{

}

QObject *ChannelEdit::GetEditedChannel() const
{
     return channel_.get();
}

void ChannelEdit::SetChannel(std::shared_ptr<ChannelObject> channel)
{
    channel_ = channel;
    emit signalChannelChanged(GetEditedChannel());
}
