#pragma once

#include <QQuickItem>

class MessagesInsert : public QQuickItem
{
    Q_OBJECT
public:
    MessagesInsert();

signals:
    void signalSendMessage(QString message);

public slots:
};
