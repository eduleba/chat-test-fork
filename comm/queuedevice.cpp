#include "queuedevice.h"

#include <iostream>

#include "common.h"

QueueDevice::QueueDevice()
    : context_(1),
      zmq_backend_(context_, ZMQ_XREQ),
      zmq_frontend_(context_, ZMQ_XREP)

{
    zmq_frontend_.bind(TCP_SOCKET_REQ);
    zmq_backend_.bind(TCP_SOCKET_REP);
}

void QueueDevice::Run()
{
    auto code = zmq_device(ZMQ_QUEUE, (void*)zmq_frontend_, (void*)zmq_backend_);
    std::cerr << "QUEUE RC: " << code << std::endl;
}
