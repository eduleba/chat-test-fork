#include "viewshelperservice.h"

ViewsHelper::ViewsHelper(QObject *parent) : QObject(parent)
{

}

void ViewsHelper::HandleAction(const Actions::ACTION_ID &action, const QVariantMap &payload)
{
    Q_UNUSED(payload)
    switch (action) {
    case Actions::SHOW_BUSY:
    {
        emit signalShowBusyPopup();
        break;
    }

    case Actions::HIDE_BUSY:
    {
        emit signalHideBusyPopup();
        break;
    }

    case Actions::SHOW_LOGIN:
    {
        emit signalShowLoginPopup();
        break;
    }

    case Actions::HIDE_LOGIN:
    {
        emit signalHideLoginPopup();
        break;
    }

    case Actions::OPEN_CHANNEL_POPUP:
    {
        emit signalOpenChannelPopup();
        break;
    }

    case Actions::OPEN_USER_POPUP:
    {
        emit signalOpenUserPopup();
        break;
    }
    default:
    {
        break;
    }
    }

}
