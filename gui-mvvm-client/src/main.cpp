#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickStyle>
#include <QQmlContext>
#include <QTimer>

#include "Services/communicationservice.h"
#include "Services/viewshelperservice.h"
#include "Services/mainrepository.h"

#include "Models/channelsmodel.h"
#include "Models/commonmodel.h"
#include "Models/messagesmodel.h"
#include "Models/usersmodel.h"

#include "ViewModels/channelsviewmodel.h"
#include "ViewModels/messagesviewmodel.h"
#include "ViewModels/usersviewmodel.h"

void RegisterComponents(){
    qRegisterMetaType<std::shared_ptr<MessageObject>>("std::shared_ptr<MessageObject>");
    qRegisterMetaType<std::shared_ptr<UserObject>>("std::shared_ptr<UserObject>");
    qRegisterMetaType<std::shared_ptr<ChannelObject>>("std::shared_ptr<ChannelObject>");
    qRegisterMetaType<std::shared_ptr<ChatStateObject>>("std::shared_ptr<ChatStateObject>");
}

int main(int argc, char *argv[])
{
    QQuickStyle::setStyle("Material");
    QCoreApplication::addLibraryPath("./");

    QGuiApplication app(argc, argv);

    RegisterComponents();

    auto main_repo = std::make_shared<MainRepository>();
    auto views_helper = std::make_shared<ViewsHelper>();
    auto comm_service = std::make_shared<CommunicationService>();

    main_repo->SetServices(views_helper,
                           comm_service);

    auto users_model = std::make_shared<UsersModel>(main_repo);
    auto channels_model = std::make_shared<ChannelsModel>(main_repo);
    auto messages_model = std::make_shared<MessagesModel>(main_repo);
    auto common_model = std::make_shared<CommonModel>(main_repo);

    main_repo->SetModels(channels_model,
                         common_model,
                         messages_model,
                         users_model);



    auto users_view_model = std::make_shared<UsersViewModel>(users_model);
    auto channels_view_model = std::make_shared<ChannelsViewModel>(channels_model);
    auto messages_view_model = std::make_shared<MessagesViewModel>(messages_model);

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty(
                     "UsersViewModel",
                     users_view_model.get());

    engine.rootContext()->setContextProperty(
                     "ChannelsViewModel",
                     channels_view_model.get());

    engine.rootContext()->setContextProperty(
                     "MessagesViewModel",
                     messages_view_model.get());

    engine.rootContext()->setContextProperty(
                     "ViewsHelper",
                     views_helper.get());

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    QTimer::singleShot(0, common_model.get(), &CommonModel::StartApp);

    return app.exec();
}

