#include <zmq.hpp>
#include <iostream>
#include <sstream>
#include <string>
#include "proto/addressbook.pb.h"
using namespace std;

// Iterates though all people in the AddressBook and prints info about them.
void ListPeople(const tutorial::AddressBook& address_book) {
  for (int i = 0; i < address_book.people_size(); i++) {
    const tutorial::Person& person = address_book.people(i);

    cout << "Person ID: " << person.id() << endl;
    cout << "  Name: " << person.name() << endl;
    if (person.has_email()) {
      cout << "  E-mail address: " << person.email() << endl;
    }

    for (int j = 0; j < person.phones_size(); j++) {
      const tutorial::Person::PhoneNumber& phone_number = person.phones(j);

      switch (phone_number.type()) {
        case tutorial::Person::MOBILE:
          cout << "  Mobile phone #: ";
          break;
        case tutorial::Person::HOME:
          cout << "  Home phone #: ";
          break;
        case tutorial::Person::WORK:
          cout << "  Work phone #: ";
          break;
      }
      cout << phone_number.number() << endl;
    }
  }
}

int main (int argc, char *argv[])
{
    GOOGLE_PROTOBUF_VERIFY_VERSION;

    zmq::context_t context (1);

    //  Socket to talk to server
    std::cout << "Collecting updates from server…\n" << std::endl;
    zmq::socket_t subscriber (context, ZMQ_SUB);
//    subscriber.connect("tcp://localhost:5556");
    subscriber.connect("ipc://test.ipc");

    subscriber.setsockopt(ZMQ_SUBSCRIBE, "", 0);

    //  Process 100 updates
    int update_nbr;
    for (update_nbr = 0; update_nbr < 100; update_nbr++) {

        zmq::message_t update;
        subscriber.recv(&update);

        std::istringstream iss(static_cast<char*>(update.data()));
        tutorial::AddressBook address_book;
          if (!address_book.ParseFromIstream(&iss)) {
            cerr << "Failed to parse address book." << endl;
            continue;
          }

        ListPeople(address_book);

    }
    google::protobuf::ShutdownProtobufLibrary();
    return 0;
}
