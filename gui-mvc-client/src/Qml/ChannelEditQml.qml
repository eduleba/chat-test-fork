import QtQuick 2.3
import QtQuick.Controls.Material 2.2
import QtQuick.Controls 2.2

import Views 1.0

ChannelEdit {
    id: idChannelAddPopup

    function open(){
        name.text = ""
        description.text = ""
        idPopup.open()
    }

    function close(){
        idPopup.close()
    }

    Popup {
        id: idPopup
        width: idChannelAddPopup.width
        height: idChannelAddPopup.height
        modal: true
        focus: true
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent
        contentItem: Item {
            Column{
                id: idColumn
                width: idPopup.width
                spacing: 20

            TextField {
                id: name
                width: idColumn.width / 2
                anchors.horizontalCenter: idColumn.horizontalCenter
                placeholderText: qsTr("Enter name")

                Material.accent: Material.Amber
            }
            TextField {
                id: description
                width: idColumn.width / 2
                anchors.horizontalCenter: idColumn.horizontalCenter
                placeholderText: qsTr("Enter description")

                Material.accent: Material.Amber

            }
            Button{
                text: "Add"
                width: idColumn.width / 4
                anchors.horizontalCenter: idColumn.horizontalCenter
                Material.background: Material.Amber
                enabled: name.text.length > 0 && description.text.length > 0
                onPressed: {
                    idChannelAddPopup.signalEditedChannel(name.text, description.text)
                    idPopup.close()
                }
            }
            }
        }
    }
}
