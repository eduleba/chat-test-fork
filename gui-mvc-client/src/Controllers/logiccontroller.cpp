#include "logiccontroller.h"

#include "communicationcontroller.h"
#include "viewscontroller.h"

#include <iostream>

LogicController::LogicController(QObject *parent)
    : QObject(parent)
    , comm_controller_(nullptr)
    , view_controller_(nullptr)
{

}

void LogicController::SetCommunicationController(std::shared_ptr<CommunicationController> comm)
{
    comm_controller_ = std::move(comm);
}

std::shared_ptr<CommunicationController> LogicController::GetCommunicationController()
{
    return comm_controller_;
}

void LogicController::SetViewsController(std::shared_ptr<ViewsController> view)
{
    view_controller_ = std::move(view);
}

std::shared_ptr<ViewsController> LogicController::GetViewsController()
{
    return view_controller_;
}

bool LogicController::ConnectControllers()
{
    if(!comm_controller_ || !view_controller_) return false;

    // Forward processing to main thread
    QObject::connect(comm_controller_.get(), &CommunicationController::signalNewChannel,
                     view_controller_.get(), &ViewsController::NewChannelUpdate, Qt::QueuedConnection);

    QObject::connect(comm_controller_.get(), &CommunicationController::signalNewUser,
                     view_controller_.get(), &ViewsController::NewUserUpdate, Qt::QueuedConnection);

    QObject::connect(comm_controller_.get(), &CommunicationController::signalNewMessage,
                     view_controller_.get(), &ViewsController::NewMessageUpdate, Qt::QueuedConnection);

    QObject::connect(comm_controller_.get(), &CommunicationController::signalNewServerState,
                     view_controller_.get(), &ViewsController::ChatStateUpdate, Qt::QueuedConnection);

    return true;
}

void LogicController::StartApp()
{
    QObject::connect(comm_controller_.get(), &CommunicationController::signalCommunicationStarted,
                     view_controller_.get(), &ViewsController::ShowLoginPopup, Qt::QueuedConnection);
    communication_start_ = std::async(std::launch::async, [this]{ comm_controller_->Start(); comm_controller_->FetchServerState();});
}
