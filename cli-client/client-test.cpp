#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

#include "proto/chat.pb.h"

#include "publisher.h"

using namespace std;

int main () {
    GOOGLE_PROTOBUF_VERIFY_VERSION;

    Publisher pub;
    pub.StartThread();

    while (true) {
        std::string msg;
        std::cin >> msg;
        if (msg.compare("exit") == 0) break;
        pub.Publish(TopicMessage("test", msg));
    }

    pub.StopThread();
    google::protobuf::ShutdownProtobufLibrary();
    return 0;
}
