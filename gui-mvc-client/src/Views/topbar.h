#pragma once

#include <memory>

#include <QQuickItem>

#include "userobject.h"
#include "channelobject.h"

class TopBar : public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(QObject* user READ GetUser NOTIFY signalUserChanged)
    Q_PROPERTY(QObject* channel READ GetChannel NOTIFY signalChannelChanged)

public:
    TopBar();

    QObject *GetUser() const;
    QObject *GetChannel() const;

signals:

    void signalUserChanged(QObject* user);
    void signalChannelChanged(QObject* channel);

public slots:
    void SetUser(std::shared_ptr<UserObject> user);
    void SetChannel(std::shared_ptr<ChannelObject> channel);

private:
    std::shared_ptr<UserObject> user_;
    std::shared_ptr<ChannelObject> channel_;
};
