#pragma once

#include <future>
#include <memory>

#include <QObject>

#include "channelobject.h"
#include "userobject.h"
#include "messageobject.h"

class CommunicationController;
class ViewsController;

class LogicController : public QObject
{
    Q_OBJECT
public:
    explicit LogicController(QObject *parent = nullptr);
    virtual ~LogicController(){}

    void SetCommunicationController(std::shared_ptr<CommunicationController> comm);
    std::shared_ptr<CommunicationController> GetCommunicationController();

    void SetViewsController(std::shared_ptr<ViewsController> view);
    std::shared_ptr<ViewsController> GetViewsController();

    bool ConnectControllers();

signals:

public slots:
    void StartApp();

private:
    std::shared_ptr<CommunicationController> comm_controller_;
    std::shared_ptr<ViewsController> view_controller_;
    std::future<void> communication_start_;
};
