#include "channelobject.h"

ChannelObject::ChannelObject(std::shared_ptr<chat::Channel> channel, QObject *parent)
    : QObject(parent)
    , channel_(channel)
{

}

QString ChannelObject::GetName() const
{
    return QString::fromStdString(channel_->name());
}

QString ChannelObject::GetDescription() const
{
    return QString::fromStdString(channel_->description());
}

std::shared_ptr<chat::Channel> ChannelObject::GetProtoBufObj() const
{
    return channel_;
}

void ChannelObject::SetName(QString name)
{
    if (GetName() == name)
        return;

    channel_->set_name(name.toStdString());
    emit nameChanged(name);
}

void ChannelObject::SetDescription(QString description)
{
    if (GetDescription() == description)
        return;

    channel_->set_description(description.toStdString());
    emit descriptionChanged(description);
}
