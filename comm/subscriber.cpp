#include "subscriber.h"

#include <iostream>
#include <sstream>

Subscriber::Subscriber(bool bind /*= false*/)
 : bind_socket_(bind), address_(TCP_SOCKET_SUB), loop_running_(false)
{
    SetupZMQ();
}

Subscriber::Subscriber(const std::string &address, bool bind /*= false*/)
 : bind_socket_(bind), address_(address), loop_running_(false)
{
    SetupZMQ();
}

Subscriber::~Subscriber()
{
    StopThread();
}

void Subscriber::StartThread()
{
    if (loop_running_) return;
    if (context_ == nullptr){
        SetupZMQ();
    }
    thread_ = std::thread(&Subscriber::SubLoop, this);
    loop_running_ = true;
    std::this_thread::sleep_for(std::chrono::seconds(1));
}

void Subscriber::SubscribeTopic(const std::string &topic)
{
    zmq_subscriber_->setsockopt(ZMQ_SUBSCRIBE, topic.c_str(), topic.size());
}

void Subscriber::UnsubscribeTopic(const std::string &topic)
{
    zmq_subscriber_->setsockopt(ZMQ_UNSUBSCRIBE, topic.c_str(), topic.size());
}

void Subscriber::SubscribeAll()
{
    zmq_subscriber_->setsockopt(ZMQ_SUBSCRIBE, "", 0);
}

void Subscriber::UnsubscribeAll()
{
    zmq_subscriber_->setsockopt(ZMQ_UNSUBSCRIBE, "", 0);
}

void Subscriber::SetupZMQ()
{
    context_.reset(new zmq::context_t(1));
    zmq_subscriber_.reset(new zmq::socket_t(*context_, ZMQ_SUB));
    if (bind_socket_){
        zmq_subscriber_->bind(address_);
    } else {
        zmq_subscriber_->connect(address_);
    }
}

void Subscriber::AddCallback(std::function<void (const TopicMessage&)> callback)
{
    callback_ = callback;
}

void Subscriber::StopThread()
{
    if (!loop_running_) return;
    loop_running_ = false;
    SubStopMessage();
    if (thread_.joinable()){
        thread_.join();
    }
}

void Subscriber::SubStopMessage()
{
    zmq_subscriber_.reset(nullptr);
    context_.reset(nullptr);
}

void Subscriber::SubLoop()
{
    try {
        while (loop_running_) {
            zmq::message_t topic;
            zmq_subscriber_->recv(&topic);
            auto topic_str = std::string(static_cast<char*>(topic.data()), topic.size());
            //        std::cout << "Topic: " << topic_str << std::endl;

            zmq::message_t payload;
            zmq_subscriber_->recv(&payload);
            auto payload_str = std::string(static_cast<char*>(payload.data()), payload.size());
            //        std::cout << "Payload: " << payload_str << std::endl;

            if (callback_){
                callback_(TopicMessage(topic_str, payload_str));
            } else {
                std::cerr << "Subscriber: Invalid callback" << std::endl;
            }
        }
    } catch (zmq::error_t & e) {
        std::cerr << "Subscriber Thread: " << e.what() << std::endl;
    }
}
