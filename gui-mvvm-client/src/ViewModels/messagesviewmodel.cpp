#include "messagesviewmodel.h"

MessagesViewModel::MessagesViewModel(std::shared_ptr<MessagesModel> messages_model_, QObject *parent)
    : QObject(parent),
      model_(messages_model_)
{
    connect(model_.get(), &MessagesModel::signalMessagesListChanged,
            [this](){emit signalModelChanged(GetModel());});
}

SimpleListModel *MessagesViewModel::GetModel() const
{
    return model_->GetListModel();
}

void MessagesViewModel::SendMessage(QString message)
{
    model_->SendMessage(message);
}
