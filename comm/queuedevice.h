#pragma once

#include <zmq.hpp>

class QueueDevice
{
public:
    QueueDevice();

    void Run();
private:
    zmq::context_t context_;
    zmq::socket_t zmq_backend_;
    zmq::socket_t zmq_frontend_;
};

