#pragma once

#include <QObject>

#include "mainrepository.h"

#include "simplelistmodel.h"
#include "channelobject.h"
#include "chatstateobject.h"

class ChannelsModel : public QObject
{
    Q_OBJECT
public:
    explicit ChannelsModel(std::shared_ptr<MainRepository> main_repo, QObject *parent = nullptr);

    SimpleListModel *GetListModel();
    ChannelObject *GetCurrentChannel();

    void SelectChannel(QString channel_name);
    void NewChannel(QString name, QString description);
    void RemoveChannel(QString name);

    void AddChannelToList(std::shared_ptr<ChannelObject> channel);

signals:
    void signalCurrentChannelChanged();
    void signalChannelsListChanged();

public slots:
    void ChatStateUpdate(const std::shared_ptr<ChatStateObject> & state);
    void NewChannelUpdate(const std::shared_ptr<ChannelObject> & channel);
    void SendNewChannel(std::shared_ptr<ChannelObject>& new_channel);

private:
    std::weak_ptr<MainRepository> main_repo_;
    std::shared_ptr<SimpleListModel> channels_list_model_;
};
