#include "customlistmodel.h"

#include <QQmlEngine>

CustomListModel::CustomListModel(QObject* parent)
    : QAbstractListModel(parent)
{

}

QQmlListProperty<QObject> CustomListModel::content()
{
    return QQmlListProperty<QObject>(this,
                                         nullptr,
                                         &CustomListModel::dataObjectAppend,
                                         &CustomListModel::dataObjectCount,
                                         &CustomListModel::dataObjectAt,
                                         &CustomListModel::dataObjectClear);
}

void CustomListModel::append(QObject *o)
{
    const int i = data_.size();
    beginInsertRows(QModelIndex(), i, i);
    data_.append(o);

    // Emit changed signals
    emit countChanged(count());

    endInsertRows();
}

void CustomListModel::insert(QObject *o, int i)
{
    beginInsertRows(QModelIndex(), i, i);
    data_.insert(i, o);

    // Emit changed signals
    emit countChanged(count());

    endInsertRows();
}

QObject *CustomListModel::get(int i)
{
    Q_ASSERT(i >= 0 && i <= data_.count());
    return data_[i];
}

int CustomListModel::count() const
{
    return data_.size();
}

int CustomListModel::rowCount(const QModelIndex &p) const
{
    Q_UNUSED(p)
    return data_.size();
}

QVariant CustomListModel::data(const QModelIndex &index, int role) const
{
    Q_UNUSED(role)
    return QVariant::fromValue(data_[index.row()]);
}

QHash<int, QByteArray> CustomListModel::roleNames() const
{
    static QHash<int, QByteArray> *pHash;
    if (!pHash) {
        pHash = new QHash<int, QByteArray>;
        (*pHash)[Qt::UserRole + 1] = "dataObject";
    }
    return *pHash;
}

void CustomListModel::dataObjectAppend(QQmlListProperty<QObject> *list, QObject *o)
{
    CustomListModel *dom = qobject_cast<CustomListModel*>(list->object);
    if (dom && o) {
        dom->append(o);
    }
}

int CustomListModel::dataObjectCount(QQmlListProperty<QObject> *list)
{
    CustomListModel *dom = qobject_cast<CustomListModel*>(list->object);
    if (dom) {
        return dom->data_.count();
    }
    return 0;
}

QObject *CustomListModel::dataObjectAt(QQmlListProperty<QObject> *list, int i)
{
    CustomListModel *dom = qobject_cast<CustomListModel*>(list->object);
    if (dom) {
        return dom->get(i);
    }
    return nullptr;
}

void CustomListModel::dataObjectClear(QQmlListProperty<QObject> *list)
{
    CustomListModel *dom = qobject_cast<CustomListModel*>(list->object);
    if (dom) {
        dom->data_.clear();
    }
}
