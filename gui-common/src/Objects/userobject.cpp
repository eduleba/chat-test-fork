#include "userobject.h"

#include <iostream>

UserObject::UserObject(std::shared_ptr<chat::User> user, QObject *parent)
    : QObject(parent)
    , user_(user)
{

}

QString UserObject::GetName() const
{
    return QString::fromStdString(user_->name());
}

QString UserObject::GetDescription() const
{
    return QString::fromStdString(user_->description());
}

std::shared_ptr<chat::User> UserObject::GetProtoBufObj() const
{
    return user_;
}

void UserObject::SetName(QString name)
{
    if (GetName() == name)
        return;

    user_->set_name(name.toStdString());
    emit nameChanged(name);
}

void UserObject::SetDescription(QString description)
{
    if (GetDescription() == description)
        return;

    user_->set_description(description.toStdString());
    emit descriptionChanged(description);
}
