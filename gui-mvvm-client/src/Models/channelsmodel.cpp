#include "channelsmodel.h"

#include "communicationservice.h"
#include "commonmodel.h"

ChannelsModel::ChannelsModel(std::shared_ptr<MainRepository> main_repo, QObject *parent)
    :  QObject(parent),
      main_repo_(main_repo)
{
    channels_list_model_ = std::make_shared<SimpleListModel>();
    // Forward processing to main thread
    QObject::connect(MainRepository::GetCommunicationService(main_repo_).get(), &CommunicationService::signalNewChannel,
                     this, &ChannelsModel::NewChannelUpdate, Qt::QueuedConnection);

    QObject::connect(MainRepository::GetCommunicationService(main_repo_).get(), &CommunicationService::signalNewServerState,
                     this, &ChannelsModel::ChatStateUpdate, Qt::QueuedConnection);

}

SimpleListModel *ChannelsModel::GetListModel()
{
    return channels_list_model_.get();
}

ChannelObject *ChannelsModel::GetCurrentChannel()
{
    return MainRepository::GetCommonModel(main_repo_)->GetCurrentChannel().get();
}

void ChannelsModel::SelectChannel(QString channel_name)
{
    MainRepository::GetCommonModel(main_repo_)->SelectChannel(channel_name);
}

void ChannelsModel::NewChannel(QString name, QString description)
{
    std::shared_ptr<chat::Channel> channel(new chat::Channel);
    channel->set_type(chat::Channel_ChannelType_PUBLIC);
    channel->set_name(name.toStdString());
    channel->set_description(description.toStdString());
    auto channel_obj = std::make_shared<ChannelObject>(channel);
    SendNewChannel(channel_obj);
}

void ChannelsModel::RemoveChannel(QString name)
{
    // TODO
}

void ChannelsModel::AddChannelToList(std::shared_ptr<ChannelObject> channel)
{
    channels_list_model_->Add(channel);
    emit signalChannelsListChanged();
}

void ChannelsModel::ChatStateUpdate(const std::shared_ptr<ChatStateObject> &state)
{
    for(auto & channel : state->GetChannels()){
        NewChannelUpdate(channel);
    }
}

void ChannelsModel::SendNewChannel(std::shared_ptr<ChannelObject> &new_channel)
{
    MainRepository::GetCommunicationService(main_repo_)->AddChannel(new_channel->GetProtoBufObj());
}

void ChannelsModel::NewChannelUpdate(const std::shared_ptr<ChannelObject> &channel)
{
    AddChannelToList(channel);
}
