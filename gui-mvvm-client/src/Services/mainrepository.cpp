#include "mainrepository.h"

MainRepository::MainRepository(QObject *parent)
    : QObject(parent)
    , channels_model_(nullptr)
    , common_model_(nullptr)
    , messages_model_(nullptr)
    , users_model_(nullptr)
    , views_helper_service_(nullptr)
    , comm_service_(nullptr)
{

}

void MainRepository::SetModels(std::shared_ptr<ChannelsModel> channels_model,
                               std::shared_ptr<CommonModel> common_model,
                               std::shared_ptr<MessagesModel> messages_model,
                               std::shared_ptr<UsersModel> users_model)
{
    channels_model_ = channels_model;
    common_model_ = common_model;
    messages_model_ = messages_model;
    users_model_ = users_model;
}

void MainRepository::SetServices(std::shared_ptr<ViewsHelper> views_helper_service,
                                 std::shared_ptr<CommunicationService> comm_service)
{
    views_helper_service_ = views_helper_service;
    comm_service_ = comm_service;
}

std::shared_ptr<ChannelsModel> MainRepository::GetChannelsModel()
{
    return channels_model_;
}

std::shared_ptr<ChannelsModel> MainRepository::GetChannelsModel(std::weak_ptr<MainRepository> repo)
{
    if (auto spt = repo.lock()){
        return spt->GetChannelsModel();
    }
    return nullptr;
}

std::shared_ptr<CommonModel> MainRepository::GetCommonModel()
{
    return common_model_;
}

std::shared_ptr<CommonModel> MainRepository::GetCommonModel(std::weak_ptr<MainRepository> repo)
{
    if (auto spt = repo.lock()){
        return spt->GetCommonModel();
    }
    return nullptr;
}

std::shared_ptr<MessagesModel> MainRepository::GetMessagesModel()
{
    return messages_model_;
}

std::shared_ptr<MessagesModel> MainRepository::GetMessagesModel(std::weak_ptr<MainRepository> repo)
{
    if (auto spt = repo.lock()){
        return spt->GetMessagesModel();
    }
    return nullptr;
}

std::shared_ptr<UsersModel> MainRepository::GetUsersModel()
{
    return users_model_;
}

std::shared_ptr<UsersModel> MainRepository::GetUsersModel(std::weak_ptr<MainRepository> repo)
{
    if (auto spt = repo.lock()){
        return spt->GetUsersModel();
    }
    return nullptr;
}

std::shared_ptr<ViewsHelper> MainRepository::GetViewsHelperService()
{
    return views_helper_service_;
}

std::shared_ptr<ViewsHelper> MainRepository::GetViewsHelperService(std::weak_ptr<MainRepository> repo)
{
    if (auto spt = repo.lock()){
        return spt->GetViewsHelperService();
    }
    return nullptr;
}

std::shared_ptr<CommunicationService> MainRepository::GetCommunicationService()
{
    return comm_service_;
}

std::shared_ptr<CommunicationService> MainRepository::GetCommunicationService(std::weak_ptr<MainRepository> repo)
{
    if (auto spt = repo.lock()){
        return spt->GetCommunicationService();
    }
    return nullptr;
}

std::shared_ptr<MainRepository> MainRepository::Get(std::weak_ptr<MainRepository> repo)
{
    if (auto spt = repo.lock()){
        return spt;
    }
    return nullptr;
}
