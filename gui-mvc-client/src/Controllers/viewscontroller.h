#pragma once

#include <QObject>
#include <QPointer>

#include "mainview.h"

#include "chatstateobject.h"

#include "logiccontroller.h"
#include "communicationcontroller.h"

class ViewsController : public QObject
{
    Q_OBJECT
public:
    explicit ViewsController(std::shared_ptr<LogicController> logic_controller,
                              QObject *parent = nullptr);
    virtual ~ViewsController(){}

    void SetMainView(QObject * root);
    QPointer<MainView> GetMainView();

signals:
    void signalSendMessage(QString new_msg);
    void signalSendMessage(std::shared_ptr<MessageObject>& new_msg);
    void signalNewUser(std::shared_ptr<UserObject>& new_user);
    void signalNewChannel(std::shared_ptr<ChannelObject>& new_channel);
    void signalSetChannel(std::shared_ptr<ChannelObject>& channel);
    void signalSetUser(std::shared_ptr<UserObject>& user);

public slots:
    void ChatStateUpdate(const std::shared_ptr<ChatStateObject> & state);
    void NewUserUpdate(const std::shared_ptr<UserObject> & user);
    void NewChannelUpdate(const std::shared_ptr<ChannelObject> & channel);
    void NewMessageUpdate(const std::shared_ptr<MessageObject> & message);

    void CurrentUserUpdate(std::shared_ptr<UserObject> & user);
    void CurrentChannelUpdate(std::shared_ptr<ChannelObject> & channel);

    void SetChannel(std::shared_ptr<ChannelObject>& channel);
    void SetUser(std::shared_ptr<UserObject>& user);

    void SendMessage(QString new_msg);
    void SendMessageObj(std::shared_ptr<MessageObject>& new_msg);
    void SendNewUser(std::shared_ptr<UserObject>& new_user);
    void SendNewChannel(std::shared_ptr<ChannelObject>& new_channel);

    void ShowLoginPopup();

    void LoginUser(std::shared_ptr<UserObject>& user);

    void SelectChannel(QString name);

private:
    void ShowCurrentChannelMessages();

    std::shared_ptr<CommunicationController> GetCommunicationController();

    std::weak_ptr<LogicController> logic_controller_;
    std::shared_ptr<ChannelObject> current_channel_;
    std::shared_ptr<UserObject> current_user_;
    QPointer<MainView> main_view_;
};
