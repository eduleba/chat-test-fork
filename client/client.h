#pragma once

#include <atomic>
#include <map>
#include <mutex>
#include <vector>

#include "subscriber.h"
#include "publisher.h"
#include "clientsocket.h"
#include "state.h"

#include "proto/chat.pb.h"

class Client
{
public:
    Client();

    void Start();
    void Stop();

    void Save(const std::string& file = "dump");
    void Load(const std::string& file = "dump");

    void ListUsers();
    void ListChannels();

    void ChangeChannel(const std::string& name);

    void AddChannel(const chat::Channel& channel);
    void AddUser(const chat::User& user);
    void SendMessage(const std::string& message);
    void SendMessage(const chat::Message& message);

    void FetchServerState();

private:
    void ShowCurrentChannelMessages();

    void SubscribeBasicTopics();

    void SubscriberCallback(const TopicMessage& message);

    void AddUserCallback(const std::string& payload);
    void UpdateUserCallback(const std::string& payload);

    void AddChannelCallback(const std::string& payload);
    void UpdateChannelCallback(const std::string& payload);

    void ChatCallback(const std::string& topic, const std::string& payload);

    void UnknownCallback(const std::string& topic, const std::string& payload);

    std::atomic<bool> expectig_setup_;

    Subscriber sub_;
    Publisher pub_;
    ClientSocket client_;
    ChatState state_;
    std::mutex lock_;

    chat::Channel current_channel;
    chat::User current_user;
};
