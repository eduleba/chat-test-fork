#include <iostream>

#include "queuedevice.h"

using namespace std;

int main ()
{
    QueueDevice que;
    std::cerr << "Queue started." << std::endl;
    que.Run();
    std::cerr << "Queue stoped." << std::endl;

    return 0;
}
