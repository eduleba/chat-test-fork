#pragma once

#include <memory>

#include <QObject>
#include <QString>

#include "proto/chat.pb.h"

class UserObject : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString name READ GetName WRITE SetName NOTIFY nameChanged)
    Q_PROPERTY(QString description READ GetDescription WRITE SetDescription NOTIFY descriptionChanged)
public:
    explicit UserObject(std::shared_ptr<chat::User> user, QObject *parent = nullptr);

    QString GetName() const;
    QString GetDescription() const;
    std::shared_ptr<chat::User> GetProtoBufObj() const;

signals:
    void nameChanged(QString name);
    void descriptionChanged(QString description);

public slots:
    void SetName(QString name);
    void SetDescription(QString description);

private:
    std::shared_ptr<chat::User> user_;
};
