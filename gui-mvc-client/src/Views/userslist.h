#pragma once

#include <memory>

#include <QQuickItem>

#include "userobject.h"
#include "simplelistmodel.h"


class UsersList : public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(SimpleListModel* users_model READ GetModel NOTIFY signalModelChanged)

public:
    UsersList();

    SimpleListModel *GetModel() const;

signals:
    void signalModelChanged(SimpleListModel* users_model);

public slots:
    void AddUserToList(std::shared_ptr<UserObject> user);

private:
    std::shared_ptr<SimpleListModel> users_model_;
};
