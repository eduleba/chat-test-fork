#pragma once

#include <memory>
#include <string>

#include "proto/chat.pb.h"
#include "common.h"

struct ChatState {
    ChatState(){}

    void WriteChatStateMessage(chat::State & state);

    void ReadChatStateMessage(const chat::State & state);

    std::vector<std::shared_ptr<chat::User>> users_;
    std::map<std::string, std::shared_ptr<chat::Channel>> topic_channel_map_;
    std::map<std::string, std::vector<std::shared_ptr<chat::Message>>> topic_messages_map_;
};
