#pragma once

#include <atomic>
#include <map>
#include <mutex>
#include <vector>

#include "mainrepository.h"
#include "subscriber.h"
#include "publisher.h"
#include "state.h"
#include "clientsocket.h"
#include "proto/chat.pb.h"
#include "action.h"

#include <QObject>

#include "chatstateobject.h"

class CommunicationService : public QObject
{
    Q_OBJECT
public:
    explicit CommunicationService(std::shared_ptr<MainRepository> main_repo, QObject *parent = nullptr);
    virtual ~CommunicationService(){}

    void Start();
    void Stop();

    void HandleAction(const Actions::ACTION_ID & action, const QVariantMap & payload);

    const ChatState GetCurrentState() const;

    void NewChannel(QString name, QString description);
    void AddChannel(const std::shared_ptr<chat::Channel>& channel);
    void NewUser(QString name, QString description);
    void AddUser(const std::shared_ptr<chat::User>& user);
    void SendMessage(QString new_msg);
    void SendMessage(const std::shared_ptr<chat::Channel>& channel,
                     const std::shared_ptr<chat::Message>& message);

    void FetchServerState();

signals:

public slots:

private:
    void SubscribeBasicTopics();

    void SubscriberCallback(const TopicMessage& message);

    void AddUserCallback(const std::string& payload);
    void UpdateUserCallback(const std::string& payload);

    void AddChannelCallback(const std::string& payload);
    void UpdateChannelCallback(const std::string& payload);

    void ChatCallback(const std::string& topic, const std::string& payload);

    void UnknownCallback(const std::string& topic, const std::string& payload);

    Subscriber sub_;
    Publisher pub_;
    ClientSocket client_;
    ChatState state_;
    std::mutex lock_;
    std::weak_ptr<MainRepository> main_repo_;
};
