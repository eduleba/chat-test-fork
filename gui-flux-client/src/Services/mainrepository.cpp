#include "mainrepository.h"

MainRepository::MainRepository(QObject *parent)
    : QObject(parent)
    , channels_store_(nullptr)
    , common_store_(nullptr)
    , messages_store_(nullptr)
    , users_store_(nullptr)
    , views_helper_service_(nullptr)
    , comm_service_(nullptr)
    , dispatcher_(nullptr)
{

}

void MainRepository::SetStores(std::shared_ptr<ChannelsStore> channels_store,
                               std::shared_ptr<CommonStore> common_store,
                               std::shared_ptr<MessagesStore> messages_store,
                               std::shared_ptr<UsersStore> users_store)
{
    channels_store_ = channels_store;
    common_store_ = common_store;
    messages_store_ = messages_store;
    users_store_ = users_store;
}

void MainRepository::SetServices(std::shared_ptr<ViewsHelper> views_helper_service,
                                 std::shared_ptr<CommunicationService> comm_service)
{
    views_helper_service_ = views_helper_service;
    comm_service_ = comm_service;
}

void MainRepository::SetDispatcher(std::shared_ptr<Dispatcher> flux_dispatcher)
{
    dispatcher_ = flux_dispatcher;
}

std::shared_ptr<ChannelsStore> MainRepository::GetChannelsStore()
{
    return channels_store_;
}

std::shared_ptr<ChannelsStore> MainRepository::GetChannelsStore(std::weak_ptr<MainRepository> repo)
{
    if (auto spt = repo.lock()){
        return spt->GetChannelsStore();
    }
    return nullptr;
}

std::shared_ptr<CommonStore> MainRepository::GetCommonStore()
{
    return common_store_;
}

std::shared_ptr<CommonStore> MainRepository::GetCommonStore(std::weak_ptr<MainRepository> repo)
{
    if (auto spt = repo.lock()){
        return spt->GetCommonStore();
    }
    return nullptr;
}

std::shared_ptr<MessagesStore> MainRepository::GetMessagesStore()
{
    return messages_store_;
}

std::shared_ptr<MessagesStore> MainRepository::GetMessagesStore(std::weak_ptr<MainRepository> repo)
{
    if (auto spt = repo.lock()){
        return spt->GetMessagesStore();
    }
    return nullptr;
}

std::shared_ptr<UsersStore> MainRepository::GetUsersStore()
{
    return users_store_;
}

std::shared_ptr<UsersStore> MainRepository::GetUsersStore(std::weak_ptr<MainRepository> repo)
{
    if (auto spt = repo.lock()){
        return spt->GetUsersStore();
    }
    return nullptr;
}

std::shared_ptr<ViewsHelper> MainRepository::GetViewsHelperService()
{
    return views_helper_service_;
}

std::shared_ptr<ViewsHelper> MainRepository::GetViewsHelperService(std::weak_ptr<MainRepository> repo)
{
    if (auto spt = repo.lock()){
        return spt->GetViewsHelperService();
    }
    return nullptr;
}

std::shared_ptr<CommunicationService> MainRepository::GetCommunicationService()
{
    return comm_service_;
}

std::shared_ptr<CommunicationService> MainRepository::GetCommunicationService(std::weak_ptr<MainRepository> repo)
{
    if (auto spt = repo.lock()){
        return spt->GetCommunicationService();
    }
    return nullptr;
}

std::shared_ptr<Dispatcher> MainRepository::GetDispatcher()
{
    return dispatcher_;
}

std::shared_ptr<Dispatcher> MainRepository::GetDispatcher(std::weak_ptr<MainRepository> repo)
{
    if (auto spt = repo.lock()){
        return spt->GetDispatcher();
    }
    return nullptr;
}

std::shared_ptr<MainRepository> MainRepository::Get(std::weak_ptr<MainRepository> repo)
{
    if (auto spt = repo.lock()){
        return spt;
    }
    return nullptr;
}
