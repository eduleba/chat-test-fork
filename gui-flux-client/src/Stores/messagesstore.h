#pragma once

#include <QObject>

#include "action.h"
#include "mainrepository.h"

#include "messageobject.h"
#include "chatstateobject.h"
#include "simplelistmodel.h"

class MessagesStore : public QObject
{
    Q_OBJECT
    Q_PROPERTY(SimpleListModel* messages_list_model READ GetListModel NOTIFY signalMessagesListChanged)
public:
    explicit MessagesStore(std::shared_ptr<MainRepository> main_repo, QObject *parent = nullptr);

    void HandleAction(const Actions::ACTION_ID & action, const QVariantMap & payload);

    SimpleListModel *GetListModel();

    void AddMessageToList(std::shared_ptr<MessageObject> msg);
    void SetMessages(std::vector<std::shared_ptr<MessageObject>> msg, bool clean = true);
    void CleanMessages();

signals:
    void signalMessagesListChanged();

public slots:
    void ChatStateUpdate(const std::shared_ptr<ChatStateObject> & state);
    void NewMessageUpdate(const std::shared_ptr<MessageObject> & message);
    void ShowCurrentChannelMessages();

private:

    std::weak_ptr<MainRepository> main_repo_;
    std::shared_ptr<SimpleListModel> messages_list_model_;
};

