#pragma once

#include <atomic>
#include <condition_variable>
#include <queue>
#include <mutex>
#include <thread>

#include <zmq.hpp>

#include "common.h"

class Publisher
{
public:
    Publisher(bool bind = false);
    Publisher(const std::string& address, bool bind = false);
    ~Publisher();

    void StartThread();
    void Publish(const TopicMessage& topic_message);
    void StopThread();

private:
    void SetupZMQ();
    void PubStopMessage();
    void PubLoop();

    bool bind_socket_;
    std::string address_;
    std::unique_ptr<zmq::context_t> context_;
    std::unique_ptr<zmq::socket_t> zmq_publisher_;
    std::atomic<bool> loop_running_;
    std::queue<TopicMessage> queue_;
    std::condition_variable condition_var_;
    std::mutex lock_;
    std::thread thread_;
};
