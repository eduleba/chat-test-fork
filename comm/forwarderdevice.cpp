#include "forwarderdevice.h"

#include <iostream>

#include "common.h"

ForwarderDevice::ForwarderDevice()
    : context_(1),
      zmq_publisher_(context_, ZMQ_PUB),
      zmq_subscriber_(context_, ZMQ_SUB)

{
    zmq_publisher_.bind(TCP_SOCKET_SUB);
    zmq_subscriber_.bind(TCP_SOCKET_PUB);
    zmq_subscriber_.setsockopt(ZMQ_SUBSCRIBE, "", 0);
}

void ForwarderDevice::Run()
{
    auto code = zmq_device(ZMQ_FORWARDER, (void*)zmq_subscriber_, (void*)zmq_publisher_);
    std::cerr << "FORWARDER RC: " << code << std::endl;
}
