#pragma once

#include <vector>
#include <map>
#include <mutex>

#include "subscriber.h"
#include "publisher.h"
#include "serversocket.h"
#include "state.h"

#include "proto/chat.pb.h"

class Server
{
public:
    Server();

    void Start();
    void Stop();

    void Save(const std::string& file = "dump");
    void Load(const std::string& file = "dump");

private:
    void SubscriberCallback(const TopicMessage& message);
    chat::Response ServerCallback(const chat::Request& req);

    void AddUserCallback(const std::string& payload);
    void UpdateUserCallback(const std::string& payload);

    void AddChannelCallback(const std::string& payload);
    void UpdateChannelCallback(const std::string& payload);

    void ChatCallback(const std::string& topic, const std::string& payload);

    void UnknownCallback(const std::string& topic, const std::string& payload);

    Subscriber sub_;
    Publisher pub_;
    ServerSocket server_;
    ChatState state_;
    std::mutex lock_;
};
