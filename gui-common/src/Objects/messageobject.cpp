#include "messageobject.h"

MessageObject::MessageObject(std::shared_ptr<chat::Message> msg, QObject *parent)
    : QObject(parent)
    , msg_(msg)
{

}

QString MessageObject::GetAuthor() const
{
    return QString::fromStdString(msg_->author());
}

QString MessageObject::GetContent() const
{
    return QString::fromStdString(msg_->content());
}

std::shared_ptr<chat::Message> MessageObject::GetProtoBufObj() const
{
    return msg_;
}

void MessageObject::SetAuthor(QString author)
{
    if (GetAuthor() == author)
        return;

    msg_->set_author(author.toStdString());
    emit authorChanged(author);
}

void MessageObject::SetContent(QString content)
{
    if (GetContent() == content)
        return;

    msg_->set_content(content.toStdString());
    emit contentChanged(content);
}
