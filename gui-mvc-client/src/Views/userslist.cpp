#include "userslist.h"

UsersList::UsersList()
    : users_model_(nullptr)
{
    users_model_ = std::make_shared<SimpleListModel>();

//    chat::User u1_proto;
//    u1_proto.set_name("test1 name");
//    u1_proto.set_description("test desc");

//    auto u1 = std::make_shared<UserObject>(u1_proto);

//    users_model_->Add(u1);

}

SimpleListModel *UsersList::GetModel() const
{
    return users_model_.get();
}

void UsersList::AddUserToList(std::shared_ptr<UserObject> user)
{
    users_model_->Add(user);
    emit signalModelChanged(GetModel());
}

