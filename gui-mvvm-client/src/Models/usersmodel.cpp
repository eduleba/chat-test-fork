#include "usersmodel.h"

#include "communicationservice.h"
#include "commonmodel.h"

UsersModel::UsersModel(std::shared_ptr<MainRepository> main_repo, QObject *parent)
    : QObject(parent),
      main_repo_(main_repo)
{
    users_list_model_ = std::make_shared<SimpleListModel>();

    // Forward processing to main thread
    QObject::connect(MainRepository::GetCommunicationService(main_repo_).get(), &CommunicationService::signalNewUser,
                     this, &UsersModel::NewUserUpdate, Qt::QueuedConnection);

    QObject::connect(MainRepository::GetCommunicationService(main_repo_).get(), &CommunicationService::signalNewServerState,
                     this, &UsersModel::ChatStateUpdate, Qt::QueuedConnection);

}

SimpleListModel *UsersModel::GetListModel()
{
    return users_list_model_.get();
}

UserObject *UsersModel::GetCurrentUser()
{
    return MainRepository::GetCommonModel(main_repo_)->GetCurrentUser().get();
}

void UsersModel::Login(QString name, QString description)
{
    std::shared_ptr<chat::User> user_proto(new chat::User);
    user_proto->set_name(name.toStdString());
    user_proto->set_description(description.toStdString());
    auto user_obj = std::make_shared<UserObject>(user_proto);
    MainRepository::GetCommonModel(main_repo_)->LoginUser(user_obj);
}

void UsersModel::NewUser(QString name, QString description)
{
    std::shared_ptr<chat::User> user_proto(new chat::User);
    user_proto->set_name(name.toStdString());
    user_proto->set_description(description.toStdString());
    auto user_obj = std::make_shared<UserObject>(user_proto);
    SendNewUser(user_obj);
}

void UsersModel::AddUserToList(std::shared_ptr<UserObject> user)
{
    users_list_model_->Add(user);
    emit signalUsersListChanged();
}

void UsersModel::ChatStateUpdate(const std::shared_ptr<ChatStateObject> &state)
{
    for(auto & user : state->GetUsers()){
        NewUserUpdate(user);
    }
}

void UsersModel::NewUserUpdate(const std::shared_ptr<UserObject> &user)
{
    AddUserToList(user);
}

void UsersModel::SendNewUser(std::shared_ptr<UserObject> &new_user)
{
    MainRepository::GetCommunicationService(main_repo_)->AddUser(new_user->GetProtoBufObj());
}
