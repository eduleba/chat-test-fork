#include <iostream>
#include <sstream>
#include <string>

#include "proto/chat.pb.h"

#include "server.h"

using namespace std;

int main ()
{
    GOOGLE_PROTOBUF_VERIFY_VERSION;

    std::cerr << "Starting server please wait..." << std::endl;
    Server server;
    std::cerr << "Loading last saved state..." << std::endl;
    server.Load();
    std::cerr << "Initializing communication..." << std::endl;
    server.Start();

    std::cerr << "Initialization complete.\nType something to stop server and save current state." << std::endl;
    std::string msg;
    std::cin >> msg;

    server.Save();
    server.Stop();

    google::protobuf::ShutdownProtobufLibrary();
    return 0;
}
