#include "channelsstore.h"

#include "communicationservice.h"
#include "commonstore.h"

Q_DECLARE_SMART_POINTER_METATYPE(std::shared_ptr)

ChannelsStore::ChannelsStore(std::shared_ptr<MainRepository> main_repo, QObject *parent)
    :  QObject(parent),
      main_repo_(main_repo)
{
    channels_list_model_ = std::make_shared<SimpleListModel>();
}

void ChannelsStore::HandleAction(const Actions::ACTION_ID &action, const QVariantMap &payload)
{
    if (action == Actions::UPDATE_CHAT_STATE){
        if (payload.contains("state") && payload["state"].canConvert<std::shared_ptr<ChatStateObject>>()) {
            ChatStateUpdate(payload["state"].value<std::shared_ptr<ChatStateObject>>());
        }
    } else if (action == Actions::ADD_CHANNEL) {
        if (payload.contains("channel") && payload["channel"].canConvert<std::shared_ptr<ChannelObject>>()) {
            NewChannelUpdate(payload["channel"].value<std::shared_ptr<ChannelObject>>());
        }
    }
}

SimpleListModel *ChannelsStore::GetListModel()
{
    return channels_list_model_.get();
}

void ChannelsStore::RemoveChannel(QString name)
{
    // TODO
}

void ChannelsStore::AddChannelToList(std::shared_ptr<ChannelObject> channel)
{
    channels_list_model_->Add(channel);
    emit signalChannelsListChanged();
}

void ChannelsStore::ChatStateUpdate(const std::shared_ptr<ChatStateObject> &state)
{
    for(auto & channel : state->GetChannels()){
        NewChannelUpdate(channel);
    }
}

void ChannelsStore::NewChannelUpdate(const std::shared_ptr<ChannelObject> &channel)
{
    AddChannelToList(channel);
}
