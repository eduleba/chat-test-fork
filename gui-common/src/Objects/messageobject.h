#pragma once

#include <memory>

#include <QObject>
#include <QString>

#include "proto/chat.pb.h"

class MessageObject : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString author READ GetAuthor WRITE SetAuthor NOTIFY authorChanged)
    Q_PROPERTY(QString content READ GetContent WRITE SetContent NOTIFY contentChanged)

public:
    explicit MessageObject(std::shared_ptr<chat::Message> msg, QObject *parent = nullptr);

    QString GetAuthor() const;
    QString GetContent() const;
    std::shared_ptr<chat::Message> GetProtoBufObj() const;

signals:
    void authorChanged(QString author);
    void contentChanged(QString content);

public slots:
    void SetAuthor(QString author);
    void SetContent(QString content);

private:
    std::shared_ptr<chat::Message> msg_;
};
