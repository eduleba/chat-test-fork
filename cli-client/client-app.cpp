#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

#include "proto/chat.pb.h"

#include "client.h"

using namespace std;

int main () {
    GOOGLE_PROTOBUF_VERIFY_VERSION;

    Client client;
    client.Start();

    client.FetchServerState();

    chat::User current_user;
    chat::Channel current_channel;
    current_channel.set_type(chat::Channel_ChannelType::Channel_ChannelType_PUBLIC);

    cout << "User name: ";
    std::cin >> *(current_user.mutable_name());
    cout << "User descriton: ";
    std::cin >> *(current_user.mutable_description());

    client.AddUser(current_user);

    cout << "####" << std::endl;
    cout << "Channel name: ";
    std::cin >> *(current_channel.mutable_name());
    cout << "Channel descriton: ";
    std::cin >> *(current_channel.mutable_description());

    client.AddChannel(current_channel);

    cout << "####" << std::endl;

    const std::string CMD_EXIT = "exit";
    const std::string CMD_LIST_USERS = ":lu:";
    const std::string CMD_LIST_CHANNELS = ":lc:";
    const std::string CMD_SET_CHANNEL = ":c:";
    const std::string CMD_NEW_CHANNEL = ":nc:";

    while (true) {
        std::cout << current_user.name() << ": ";
        std::string msg;
        std::cin >> msg;
        if (msg.compare(CMD_EXIT) == 0) {
            break;
        } else if(msg.compare(CMD_LIST_USERS) == 0) {
            client.ListUsers();
            continue;
        } else if(msg.compare(CMD_LIST_CHANNELS) == 0) {
            client.ListChannels();
            continue;
        } else if(msg.compare(CMD_SET_CHANNEL) == 0) {
            std::cin >> msg;
            client.ChangeChannel(msg);
            continue;
        } else if(msg.compare(CMD_NEW_CHANNEL) == 0) {
            cout << "Channel name: ";
            std::cin >> *(current_channel.mutable_name());
            cout << "Channel descriton: ";
            std::cin >> *(current_channel.mutable_description());

            client.AddChannel(current_channel);
            continue;
        }

        client.SendMessage(msg);
    }

    client.Stop();

    google::protobuf::ShutdownProtobufLibrary();
    return 0;
}
