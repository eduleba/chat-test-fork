#include "usersviewmodel.h"

UsersViewModel::UsersViewModel(std::shared_ptr<UsersModel> users_model, QObject *parent)
    : QObject(parent),
      model_(users_model)
{
    connect(model_.get(), &UsersModel::signalCurrentUserChanged,
            [this](){emit signalUserChanged(GetCurrentUser());});

    connect(model_.get(), &UsersModel::signalUsersListChanged,
            [this](){emit signalModelChanged(GetModel());});
}

SimpleListModel *UsersViewModel::GetModel() const
{
    return model_->GetListModel();
}

UserObject *UsersViewModel::GetCurrentUser() const
{
    return model_->GetCurrentUser();
}

void UsersViewModel::Login(QString name, QString description)
{
    model_->Login(name, description);
}

void UsersViewModel::NewUser(QString name, QString description)
{
    model_->NewUser(name, description);
}

void UsersViewModel::AddUser()
{
    emit signalAddUser();
}
