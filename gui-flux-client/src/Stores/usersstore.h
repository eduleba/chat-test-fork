#pragma once

#include <QObject>

#include "action.h"
#include "mainrepository.h"

#include "simplelistmodel.h"
#include "chatstateobject.h"

class UsersStore : public QObject
{
    Q_OBJECT
    Q_PROPERTY(SimpleListModel* users_list_model READ GetListModel NOTIFY signalUsersListChanged)
public:
    explicit UsersStore(std::shared_ptr<MainRepository> main_repo, QObject *parent = nullptr);

    void HandleAction(const Actions::ACTION_ID & action, const QVariantMap & payload);

    SimpleListModel *GetListModel();

    void AddUserToList(std::shared_ptr<UserObject> user);

signals:
    void signalUsersListChanged();

public slots:
    void ChatStateUpdate(const std::shared_ptr<ChatStateObject> & state);
    void NewUserUpdate(const std::shared_ptr<UserObject> & user);

private:
    std::weak_ptr<MainRepository> main_repo_;
    std::shared_ptr<SimpleListModel> users_list_model_;
};
