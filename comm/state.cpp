#include "state.h"


void ChatState::WriteChatStateMessage(chat::State & state){
    for(auto & user : users_){
        auto new_user = state.add_users();
        *new_user = *user;
    }
    for(auto topic_channel : topic_channel_map_){
        auto new_channel = state.add_channels();
        *new_channel = *topic_channel.second;
        auto messages = topic_messages_map_.find(topic_channel.first);
        if (messages != topic_messages_map_.end()){
            for(auto & msg : messages->second){
                auto message = new_channel->add_messages();
                *message = *msg;
            }
        }
    }
}

void ChatState::ReadChatStateMessage(const chat::State & state){
    users_.clear();
    for(int i = 0; i < state.users_size(); ++i){
        users_.push_back(std::make_shared<chat::User>(state.users(i)));
    }

    topic_channel_map_.clear();
    topic_messages_map_.clear();
    for(int i = 0; i < state.channels_size(); ++i){
        auto channel = state.channels(i);
        std::string topic = ParseChannelTopic(channel);
        if (channel.messages_size() > 0){
            std::vector<std::shared_ptr<chat::Message>> chat_messages;
            for(int j = 0; j < channel.messages_size(); ++j){
                chat_messages.push_back(std::make_shared<chat::Message>(channel.messages(j)));
            }
            topic_messages_map_.insert({topic, chat_messages});
        }
        channel.clear_messages();
        topic_channel_map_.insert({topic, std::make_shared<chat::Channel>(channel)});
    }
}
