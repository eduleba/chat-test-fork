#pragma once

// https://github.com/ndesai/qml.guide/tree/master/examples/DataObjectModel

#include <QAbstractListModel>
#include <QQmlListProperty>

class CustomListModel : public QAbstractListModel
{
    Q_OBJECT
    Q_DISABLE_COPY(CustomListModel)
    Q_PROPERTY(int count READ count NOTIFY countChanged)
    Q_PROPERTY(QQmlListProperty<QObject> content READ content)
    Q_CLASSINFO("DefaultProperty", "content")

public:
    explicit CustomListModel(QObject* parent = nullptr);
    QQmlListProperty<QObject> content();

    int count() const;

    int rowCount(const QModelIndex &p) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;

signals:
    void countChanged(int count);

public slots:
    Q_INVOKABLE void append(QObject* o);
    Q_INVOKABLE void insert(QObject* o, int i);
    Q_INVOKABLE QObject* get(int i);

    static void dataObjectAppend(QQmlListProperty<QObject> *list, QObject *e);
    static int dataObjectCount(QQmlListProperty<QObject> *list);
    static QObject* dataObjectAt(QQmlListProperty<QObject> *list, int i);
    static void dataObjectClear(QQmlListProperty<QObject> *list);

private:
    QList<QObject*> data_;
};
