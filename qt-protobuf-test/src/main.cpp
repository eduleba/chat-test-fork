#include <QGuiApplication>

#include <QTimer>

#include "proto/chat.pb.h"
#include "publisher.h"

int main(int argc, char *argv[])
{
    chat::Message mgs;
    Publisher pub;
    QGuiApplication::addLibraryPath("./");
    QGuiApplication app(argc, argv);

    QTimer::singleShot(2000, &app, &QGuiApplication::quit);
    return app.exec();
}

