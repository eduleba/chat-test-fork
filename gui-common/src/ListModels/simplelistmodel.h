#pragma once

#include <memory>

#include <QAbstractListModel>
#include <QQmlListProperty>

class SimpleListModel : public QAbstractListModel
{
    Q_OBJECT
    Q_DISABLE_COPY(SimpleListModel)

public:
    explicit SimpleListModel(QObject* parent = nullptr);

    void Add(std::shared_ptr<QObject> item);

    void Clean();

    std::shared_ptr<QObject> Get(int index);
    std::shared_ptr<QObject> GetByProperty(QString poperty, QVariant value);

    int rowCount(const QModelIndex &p) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;

signals:
    void countChanged(int count);

public slots:
//    Q_INVOKABLE void append(QObject* o);
//    Q_INVOKABLE void insert(QObject* o, int i);
//    Q_INVOKABLE QObject* get(int i);

private:
    void PrepareRolesMap(std::shared_ptr<QObject> item);

    QHash<int, QByteArray> roles_map_;
    QList<std::shared_ptr<QObject>> data_;
};
