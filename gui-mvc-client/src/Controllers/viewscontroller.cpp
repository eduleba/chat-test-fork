#include "viewscontroller.h"

#include <iostream>

#include "proto/chat.pb.h"
#include "common.h"

ViewsController::ViewsController(std::shared_ptr<LogicController> logic_controller,
                                 QObject *parent)
    : QObject(parent)
    , logic_controller_(logic_controller)
    , current_channel_(nullptr)
    , current_user_(nullptr)
    , main_view_(nullptr)
{

}

void ViewsController::SetMainView(QObject *root)
{
    main_view_ = root->findChild<MainView*>(QString(), Qt::FindDirectChildrenOnly);
    connect(main_view_, &MainView::signalLoginUser,
            this, &ViewsController::LoginUser);

    connect(main_view_, &MainView::signalAddChannel,
            this, &ViewsController::SendNewChannel);

    connect(main_view_, &MainView::signalSelectChannel,
            this, &ViewsController::SelectChannel);

    connect(main_view_, &MainView::signalSendMessage,
            this, &ViewsController::SendMessage);
}

QPointer<MainView> ViewsController::GetMainView()
{
    return main_view_;
}

void ViewsController::ChatStateUpdate(const std::shared_ptr<ChatStateObject> &state)
{
    for(auto & user : state->GetUsers()){
        NewUserUpdate(user);
    }
    for(auto & channel : state->GetChannels()){
        NewChannelUpdate(channel);
    }
    // TODO: use channels messages
}

void ViewsController::NewUserUpdate(const std::shared_ptr<UserObject> &user)
{
    auto users_list = main_view_->GetUsersList();
    users_list->AddUserToList(user);
}

void ViewsController::NewChannelUpdate(const std::shared_ptr<ChannelObject> &channel)
{
    auto channels_list = main_view_->GetChannelsList();
    channels_list->AddChannelToList(channel);
}

void ViewsController::NewMessageUpdate(const std::shared_ptr<MessageObject> &message)
{
//    if (!current_user_) return;
//    if (message && message->GetAuthor() != current_user_->GetName()){
        auto messages_view = main_view_->GetMessagesView();
        messages_view->AddMessageToList(message);
//    }
}

void ViewsController::CurrentUserUpdate(std::shared_ptr<UserObject> &user)
{
    SetUser(user);
}

void ViewsController::CurrentChannelUpdate(std::shared_ptr<ChannelObject> &channel)
{
    SetChannel(channel);
}

void ViewsController::SetChannel(std::shared_ptr<ChannelObject> &channel)
{
    current_channel_ = channel;
    auto topBar = main_view_->GetTopBar();
    topBar->SetChannel(channel);
}

void ViewsController::SetUser(std::shared_ptr<UserObject> &user)
{
    current_user_ = user;
    auto topBar = main_view_->GetTopBar();
    topBar->SetUser(user);
}

void ViewsController::SendMessage(QString new_msg)
{
    std::shared_ptr<chat::Message> msg(new chat::Message);
    msg->set_ms_since_epoch_utc(1); // TODO: fixme
    msg->set_author(current_user_->GetName().toStdString());
    msg->set_content(new_msg.toStdString());
    GetCommunicationController()->SendMessage(current_channel_->GetProtoBufObj(), msg);
}

void ViewsController::SendMessageObj(std::shared_ptr<MessageObject> &new_msg)
{
    GetCommunicationController()->SendMessage(current_channel_->GetProtoBufObj(), new_msg->GetProtoBufObj());
}

void ViewsController::SendNewUser(std::shared_ptr<UserObject> &new_user)
{
    GetCommunicationController()->AddUser(new_user->GetProtoBufObj());
}

void ViewsController::SendNewChannel(std::shared_ptr<ChannelObject> &new_channel)
{
    GetCommunicationController()->AddChannel(new_channel->GetProtoBufObj());
}

void ViewsController::ShowLoginPopup()
{
    main_view_->HideBusyPopup();
    auto login_popup = main_view_->GetLoginPopup();
    login_popup->Show();
}

void ViewsController::LoginUser(std::shared_ptr<UserObject> &user)
{
    SendNewUser(user);
    SetUser(user);
}

void ViewsController::SelectChannel(QString name)
{
    auto & state = GetCommunicationController()->GetCurrentState();
    for(auto & channel : state.topic_channel_map_){
        if (channel.second->name() == name.toStdString()){
            auto channel_obj = std::make_shared<ChannelObject>(channel.second);
            SetChannel(channel_obj);
            ShowCurrentChannelMessages();
            break;
        }
    }
}

void ViewsController::ShowCurrentChannelMessages()
{
    auto & state = GetCommunicationController()->GetCurrentState();
    auto messages_list = main_view_->GetMessagesView();
    auto channel = state.topic_messages_map_.find(ParseChannelTopic(*current_channel_->GetProtoBufObj()));
    if(channel != state.topic_messages_map_.end() && !channel->second.empty()){
        std::vector <std::shared_ptr<MessageObject>> messages;
        for(auto & msg : channel->second){
            messages.push_back(std::make_shared<MessageObject>(msg));
        }
        messages_list->SetMessages(messages, true);
    } else {
        messages_list->CleanMessages();
    }
}

std::shared_ptr<CommunicationController> ViewsController::GetCommunicationController()
{
    if (auto spt = logic_controller_.lock()){
        return spt->GetCommunicationController();
    }
    return nullptr;
}
