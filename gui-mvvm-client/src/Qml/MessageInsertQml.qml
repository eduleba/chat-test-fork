import QtQuick 2.3
import QtQuick.Controls.Material 2.2
import QtQuick.Controls 2.2

Item {
    id: idMessagesInsert

    Rectangle {
        id: idFrame
        anchors.left: idMessagesInsert.left
        anchors.top: idMessagesInsert.top
        anchors.bottom: idMessagesInsert.bottom
        anchors.right: idSendButton.left
        anchors.margins: 5
        color: "transparent"
        border.color: "gray"
        radius: 3
    }

    TextEdit{
        id: idTextEdit
        anchors.left: idMessagesInsert.left
        anchors.top: idMessagesInsert.top
        anchors.bottom: idMessagesInsert.bottom
        anchors.right: idSendButton.left
        anchors.margins: 10
        wrapMode: Text.WordWrap
    }

    RoundButton{
        id: idSendButton
        height: idMessagesInsert.height - 10
        width: idSendButton.height
        anchors.right: idMessagesInsert.right
        anchors.top: idMessagesInsert.top
        anchors.bottom: idMessagesInsert.bottom
        anchors.margins: 5

        enabled: idTextEdit.text.length > 0

        Material.background: Material.Amber

        text: "\u2713"
        onClicked: {
            MessagesViewModel.SendMessage(idTextEdit.text)
            idTextEdit.text = ""
        }
    }
}
