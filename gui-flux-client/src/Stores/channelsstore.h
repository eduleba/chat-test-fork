#pragma once

#include <QObject>

#include "action.h"
#include "mainrepository.h"

#include "simplelistmodel.h"
#include "channelobject.h"
#include "chatstateobject.h"

class ChannelsStore : public QObject
{
    Q_OBJECT
    Q_PROPERTY(SimpleListModel* channels_list_model READ GetListModel NOTIFY signalChannelsListChanged)
public:
    explicit ChannelsStore(std::shared_ptr<MainRepository> main_repo, QObject *parent = nullptr);

    void HandleAction(const Actions::ACTION_ID & action, const QVariantMap & payload);

    SimpleListModel *GetListModel();

    void RemoveChannel(QString name);

    void AddChannelToList(std::shared_ptr<ChannelObject> channel);

signals:
    void signalChannelsListChanged();

public slots:
    void ChatStateUpdate(const std::shared_ptr<ChatStateObject> & state);
    void NewChannelUpdate(const std::shared_ptr<ChannelObject> & channel);

private:
    std::weak_ptr<MainRepository> main_repo_;
    std::shared_ptr<SimpleListModel> channels_list_model_;
};
