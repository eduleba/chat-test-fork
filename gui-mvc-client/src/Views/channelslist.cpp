#include "channelslist.h"

ChannelsList::ChannelsList()
    : channels_model_(nullptr)
{
    channels_model_ = std::make_shared<SimpleListModel>();
}

SimpleListModel *ChannelsList::GetModel() const
{
    return channels_model_.get();
}

void ChannelsList::SelectChannel(QString channel_name)
{
//   auto channel_item = channels_model_->GetByProperty("name", channel_name);

//   if (channel_item == nullptr) return;

//   auto channel = std::dynamic_pointer_cast<ChannelObject>(channel_item);
   emit signalSelectChannel(channel_name);
}

void ChannelsList::AddChannelToList(std::shared_ptr<ChannelObject> channel)
{
    channels_model_->Add(channel);
    emit signalModelChanged(GetModel());
}
