#include "common.h"

std::string ParseChannelTopic(const chat::Channel & channel){
    return TOPIC_CHANNEL_PREFIX + channel.name();
}
