#pragma once

#include <memory>

#include <QObject>

class ChannelsStore;
class CommonStore;
class MessagesStore;
class UsersStore;
class ViewsHelper;
class CommunicationService;
class Dispatcher;


class MainRepository : public QObject
{
    Q_OBJECT
public:
    explicit MainRepository(QObject *parent = nullptr);

    void SetStores(std::shared_ptr<ChannelsStore> channels_store,
                   std::shared_ptr<CommonStore> common_store,
                   std::shared_ptr<MessagesStore> messages_store,
                   std::shared_ptr<UsersStore> users_store);

    void SetServices(std::shared_ptr<ViewsHelper> views_helper_service,
                     std::shared_ptr<CommunicationService> comm_service);

    void SetDispatcher(std::shared_ptr<Dispatcher> flux_dispatcher);

    std::shared_ptr<ChannelsStore> GetChannelsStore();
    static std::shared_ptr<ChannelsStore> GetChannelsStore(std::weak_ptr<MainRepository> repo);

    std::shared_ptr<CommonStore> GetCommonStore();
    static std::shared_ptr<CommonStore> GetCommonStore(std::weak_ptr<MainRepository> repo);

    std::shared_ptr<MessagesStore> GetMessagesStore();
    static std::shared_ptr<MessagesStore> GetMessagesStore(std::weak_ptr<MainRepository> repo);

    std::shared_ptr<UsersStore> GetUsersStore();
    static std::shared_ptr<UsersStore> GetUsersStore(std::weak_ptr<MainRepository> repo);

    std::shared_ptr<ViewsHelper> GetViewsHelperService();
    static std::shared_ptr<ViewsHelper> GetViewsHelperService(std::weak_ptr<MainRepository> repo);

    std::shared_ptr<CommunicationService> GetCommunicationService();
    static std::shared_ptr<CommunicationService> GetCommunicationService(std::weak_ptr<MainRepository> repo);

    std::shared_ptr<Dispatcher> GetDispatcher();
    static std::shared_ptr<Dispatcher> GetDispatcher(std::weak_ptr<MainRepository> repo);


    static std::shared_ptr<MainRepository> Get(std::weak_ptr<MainRepository> repo);

signals:

public slots:
private:
    std::shared_ptr<ChannelsStore> channels_store_;
    std::shared_ptr<CommonStore> common_store_;
    std::shared_ptr<MessagesStore> messages_store_;
    std::shared_ptr<UsersStore> users_store_;
    std::shared_ptr<ViewsHelper> views_helper_service_;
    std::shared_ptr<CommunicationService> comm_service_;
    std::shared_ptr<Dispatcher> dispatcher_;
};
