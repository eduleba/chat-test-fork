#pragma once

#include <QQuickItem>
#include <QPointer>

#include "channeledit.h"
#include "topbar.h"
#include "channelslist.h"
#include "messagesinsert.h"
#include "useredit.h"
#include "loginpopup.h"
#include "messagesview.h"
#include "userslist.h"

class MainView : public QQuickItem
{
    Q_OBJECT
public:
    MainView();

    void ShowBusyPopup();
    void HideBusyPopup();

    Q_INVOKABLE void SetChannelEditPopup(QQuickItem * item);
    QPointer<ChannelEdit> GetChannelEditPopup();

    Q_INVOKABLE void SetTopBar(QQuickItem * item);
    QPointer<TopBar> GetTopBar();

    Q_INVOKABLE void SetChannelsList(QQuickItem * item);
    QPointer<ChannelsList> GetChannelsList();

    Q_INVOKABLE void SetMessagesInsert(QQuickItem * item);
    QPointer<MessagesInsert> GetMessagesInsert();

    Q_INVOKABLE void SetUserEditPopup(QQuickItem * item);
    QPointer<UserEdit> GetUserEditPopup();

    Q_INVOKABLE void SetLoginPopup(QQuickItem * item);
    QPointer<LoginPopup> GetLoginPopup();

    Q_INVOKABLE void SetMessagesView(QQuickItem * item);
    QPointer<MessagesView> GetMessagesView();

    Q_INVOKABLE void SetUsersList(QQuickItem * item);
    QPointer<UsersList> GetUsersList();

signals:
    void signalLoginUser(std::shared_ptr<UserObject>& user);
    void signalAddChannel(std::shared_ptr<ChannelObject>& channel);
    void signalSelectChannel(QString msg);
    void signalSendMessage(QString msg);

public slots:
    void Login(QString name, QString description);
    void NewChannel(QString name, QString description);

private:
    QPointer<ChannelEdit> channel_edit_popup_;
    QPointer<TopBar> top_bar_;
    QPointer<ChannelsList> channels_list_;
    QPointer<MessagesInsert> message_insert_;
    QPointer<UserEdit> user_edit_popup_;
    QPointer<LoginPopup> login_popup_;
    QPointer<MessagesView> messages_view_;
    QPointer<UsersList> users_list_;
};
