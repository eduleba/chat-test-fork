#pragma once

#include <memory>

#include <QQuickItem>

#include "channelobject.h"
#include "simplelistmodel.h"

class ChannelsList : public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(SimpleListModel* channels_model READ GetModel NOTIFY signalModelChanged)
public:
    ChannelsList();

    SimpleListModel *GetModel() const;
    Q_INVOKABLE void SelectChannel(QString channel_name);


signals:
    void signalModelChanged(SimpleListModel* users_model);
    void signalSelectChannel(QString name);

public slots:
    void AddChannelToList(std::shared_ptr<ChannelObject> channel);

private:

    std::shared_ptr<SimpleListModel> channels_model_;
};
