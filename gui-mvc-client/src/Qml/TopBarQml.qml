import QtQuick 2.3
import QtQuick.Controls.Material 2.2
import QtQuick.Controls 2.2

import Views 1.0

TopBar {
    id: idTopBar

    Row {
        id: idTopBarRow
        anchors.fill: idTopBar
        Item {
            id: idCurrentChannel
            anchors.verticalCenter: idTopBarRow.verticalCenter
            width: (idTopBarRow.width - idButtonSettings.width - 10) / 2
            height: idTopBarRow.height
            Column {
                id: idCurrentChannelCol
                width: idCurrentChannel.width
                Text {
                    id: idCurrentChannelTitle
                    text: qsTr("Current Channel")
                    font.bold: true
                }
                Text {
                    id: idCurrentChannelName
                    text: idTopBar.channel !== null ? idTopBar.channel.name
                                                    : ""
                }
                Text {
                    id: idCurrentChannelDescription
                    text: idTopBar.channel !== null ? idTopBar.channel.description
                                                    : ""
                    font.italic: true
                }
            }
        }
        Item {
            id: idCurrentUser
            anchors.verticalCenter: idTopBarRow.verticalCenter
            width: (idTopBarRow.width - idButtonSettings.width - 10) / 2
            height: idTopBarRow.height
            Column {
                id: idCurrentUserCol
                width: idCurrentChannel.width
                Text {
                    id: idCurrentUserTitle
                    text: qsTr("Current User")
                    font.bold: true
                }
                Text {
                    id: idCurrentUserName
                    text: idTopBar.user !== null ? idTopBar.user.name
                                                 : ""
                }
                Text {
                    id: idCurrentUserDescription
                    text: idTopBar.user !== null ? idTopBar.user.description
                                                 : ""
                    font.italic: true
                }
            }
        }
        Button {
            id: idButtonSettings
            anchors.verticalCenter: idTopBarRow.verticalCenter
            width: 100

            text: qsTr("settings")
            Material.background: Material.Amber
        }
    }
}
