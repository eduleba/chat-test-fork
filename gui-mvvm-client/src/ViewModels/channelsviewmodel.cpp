#include "channelsviewmodel.h"

ChannelsViewModel::ChannelsViewModel(std::shared_ptr<ChannelsModel> channels_model, QObject *parent)
    : QObject(parent),
      model_(channels_model)
{
    connect(model_.get(), &ChannelsModel::signalCurrentChannelChanged,
            [this](){emit signalChannelChanged(GetCurrentChannel());});

    connect(model_.get(), &ChannelsModel::signalChannelsListChanged,
            [this](){emit signalModelChanged(GetModel());});
}

SimpleListModel *ChannelsViewModel::GetModel() const
{
    return model_->GetListModel();
}

void ChannelsViewModel::SelectChannel(QString channel_name)
{
    model_->SelectChannel(channel_name);
}

void ChannelsViewModel::NewChannel(QString name, QString description)
{
    model_->NewChannel(name, description);
}

void ChannelsViewModel::AddChannel()
{
    emit signalAddChannel();
}

void ChannelsViewModel::RemoveChannel(QString name)
{
    model_->RemoveChannel(name);
}

ChannelObject *ChannelsViewModel::GetCurrentChannel() const
{
    return model_->GetCurrentChannel();
}

