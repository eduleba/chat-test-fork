#pragma once

#include <memory>

#include <QQuickItem>

#include "messageobject.h"
#include "simplelistmodel.h"

class MessagesView : public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(SimpleListModel* messages_model READ GetModel NOTIFY signalModelChanged)
public:
    MessagesView();

    SimpleListModel *GetModel() const;

signals:
    void signalModelChanged(SimpleListModel* users_model);

public slots:
    void AddMessageToList(std::shared_ptr<MessageObject> msg);
    void SetMessages(std::vector<std::shared_ptr<MessageObject>> msg, bool clean = true);
    void CleanMessages();

private:
    std::shared_ptr<SimpleListModel> messages_model_;
};
