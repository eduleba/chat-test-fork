#pragma once

#include <memory>

#include <QObject>

class ChannelsModel;
class CommonModel;
class MessagesModel;
class UsersModel;
class ViewsHelper;
class CommunicationService;


class MainRepository : public QObject
{
    Q_OBJECT
public:
    explicit MainRepository(QObject *parent = nullptr);

    void SetModels(std::shared_ptr<ChannelsModel> channels_model,
                   std::shared_ptr<CommonModel> common_model,
                   std::shared_ptr<MessagesModel> messages_model,
                   std::shared_ptr<UsersModel> users_model);

    void SetServices(std::shared_ptr<ViewsHelper> views_helper_service,
                     std::shared_ptr<CommunicationService> comm_service);

    std::shared_ptr<ChannelsModel> GetChannelsModel();
    static std::shared_ptr<ChannelsModel> GetChannelsModel(std::weak_ptr<MainRepository> repo);

    std::shared_ptr<CommonModel> GetCommonModel();
    static std::shared_ptr<CommonModel> GetCommonModel(std::weak_ptr<MainRepository> repo);

    std::shared_ptr<MessagesModel> GetMessagesModel();
    static std::shared_ptr<MessagesModel> GetMessagesModel(std::weak_ptr<MainRepository> repo);

    std::shared_ptr<UsersModel> GetUsersModel();
    static std::shared_ptr<UsersModel> GetUsersModel(std::weak_ptr<MainRepository> repo);

    std::shared_ptr<ViewsHelper> GetViewsHelperService();
    static std::shared_ptr<ViewsHelper> GetViewsHelperService(std::weak_ptr<MainRepository> repo);

    std::shared_ptr<CommunicationService> GetCommunicationService();
    static std::shared_ptr<CommunicationService> GetCommunicationService(std::weak_ptr<MainRepository> repo);

    static std::shared_ptr<MainRepository> Get(std::weak_ptr<MainRepository> repo);

signals:

public slots:
private:
    std::shared_ptr<ChannelsModel> channels_model_;
    std::shared_ptr<CommonModel> common_model_;
    std::shared_ptr<MessagesModel> messages_model_;
    std::shared_ptr<UsersModel> users_model_;
    std::shared_ptr<ViewsHelper> views_helper_service_;
    std::shared_ptr<CommunicationService> comm_service_;
};
