#pragma once

#include <memory>

#include <QObject>

#include "usersmodel.h"
#include "simplelistmodel.h"

class UsersViewModel : public QObject
{
    Q_OBJECT
    Q_PROPERTY(SimpleListModel* users_list_model READ GetModel NOTIFY signalModelChanged)
    Q_PROPERTY(UserObject* current_user READ GetCurrentUser NOTIFY signalUserChanged)
public:
    explicit UsersViewModel(std::shared_ptr<UsersModel> users_model, QObject *parent = nullptr);

    SimpleListModel* GetModel() const;
    UserObject* GetCurrentUser() const;

    Q_INVOKABLE void Login(QString name, QString description);
    Q_INVOKABLE void NewUser(QString name, QString description);
    Q_INVOKABLE void AddUser();

signals:
    void signalModelChanged(SimpleListModel* users_list_model);
    void signalUserChanged(UserObject* current_user);
    void signalAddUser();

public slots:

private:
    std::shared_ptr<UsersModel> model_;
};
