#include "mainview.h"

#include <iostream>

MainView::MainView()
    : channel_edit_popup_(nullptr)
    , top_bar_(nullptr)
    , channels_list_(nullptr)
    , message_insert_(nullptr)
    , user_edit_popup_(nullptr)
    , login_popup_(nullptr)
    , messages_view_(nullptr)
    , users_list_(nullptr)
{

}

void MainView::ShowBusyPopup()
{
    QMetaObject::invokeMethod(this, "openBusyPopup");
}

void MainView::HideBusyPopup()
{
    QMetaObject::invokeMethod(this, "closeBusyPopup");
}

void MainView::SetChannelEditPopup(QQuickItem *item)
{
    channel_edit_popup_ = dynamic_cast<ChannelEdit*>(item);
    connect(channel_edit_popup_, &ChannelEdit::signalEditedChannel,
            this, &MainView::NewChannel);
}

QPointer<ChannelEdit> MainView::GetChannelEditPopup()
{
    return  channel_edit_popup_;
}

void MainView::SetTopBar(QQuickItem *item)
{
    top_bar_ = dynamic_cast<TopBar*>(item);
}

QPointer<TopBar> MainView::GetTopBar()
{
    return  top_bar_;
}

void MainView::SetChannelsList(QQuickItem *item)
{
    channels_list_ = dynamic_cast<ChannelsList*>(item);
    connect(channels_list_, &ChannelsList::signalSelectChannel,
            this, &MainView::signalSelectChannel);
}

QPointer<ChannelsList> MainView::GetChannelsList()
{
    return  channels_list_;
}

void MainView::SetMessagesInsert(QQuickItem *item)
{
    message_insert_ = dynamic_cast<MessagesInsert*>(item);
    connect(message_insert_, &MessagesInsert::signalSendMessage,
            this, &MainView::signalSendMessage);
}

QPointer<MessagesInsert> MainView::GetMessagesInsert()
{
    return  message_insert_;
}

void MainView::SetUserEditPopup(QQuickItem *item)
{
    user_edit_popup_ = dynamic_cast<UserEdit*>(item);
}

QPointer<UserEdit> MainView::GetUserEditPopup()
{
    return user_edit_popup_;
}

void MainView::SetLoginPopup(QQuickItem *item)
{
    login_popup_ = dynamic_cast<LoginPopup*>(item);
    connect(login_popup_, &LoginPopup::signalLogin,
            this, &MainView::Login);
}

QPointer<LoginPopup> MainView::GetLoginPopup()
{
    return login_popup_;
}

void MainView::SetMessagesView(QQuickItem *item)
{
    messages_view_ = dynamic_cast<MessagesView*>(item);
}

QPointer<MessagesView> MainView::GetMessagesView()
{
    return  messages_view_;
}

void MainView::SetUsersList(QQuickItem *item)
{
    users_list_ = dynamic_cast<UsersList*>(item);
}

QPointer<UsersList> MainView::GetUsersList()
{
    return users_list_;
}

void MainView::Login(QString name, QString description)
{
    std::shared_ptr<chat::User> u(new chat::User);
    u->set_name(name.toStdString());
    u->set_description(description.toStdString());
    auto u_obj = std::make_shared<UserObject>(u);
    emit signalLoginUser(u_obj);
}

void MainView::NewChannel(QString name, QString description)
{
   std::shared_ptr<chat::Channel> c(new chat::Channel);
    c->set_name(name.toStdString());
    c->set_description(description.toStdString());
    c->set_type(chat::Channel_ChannelType::Channel_ChannelType_PUBLIC);
    auto c_obj = std::make_shared<ChannelObject>(c);
    emit signalAddChannel(c_obj);
}
