#include "server.h"
#include <iostream>
#include <fstream>

Server::Server()
{

}

void Server::Start()
{
    pub_.StartThread();
    sub_.SubscribeAll();
    sub_.AddCallback(std::bind(&Server::SubscriberCallback, this, std::placeholders::_1));
    sub_.StartThread();
    server_.StartThread();
    server_.AddCallback(std::bind(&Server::ServerCallback, this, std::placeholders::_1));
    std::this_thread::sleep_for(std::chrono::seconds(1));
}

void Server::Stop()
{
    pub_.StopThread();
    sub_.StopThread();
}

void Server::Save(const std::string& file /*= "dump"*/)
{
    std::lock_guard<std::mutex> lock(lock_);
    chat::State state;
    state_.WriteChatStateMessage(state);
    {
      std::fstream output(file, std::ios::out | std::ios::trunc | std::ios::binary);
      if (!state.SerializeToOstream(&output)) {
        std::cerr << "Failed to save state." << std::endl;
      }
    }
}

void Server::Load(const std::string &file /*= "dump"*/)
{
    std::lock_guard<std::mutex> lock(lock_);
    chat::State state;
    std::fstream input(file, std::ios::in | std::ios::binary);
    if (!input) {
      std::cerr << "Invalid file: " << file << std::endl;
    } else if (!state.ParseFromIstream(&input)) {
      std::cerr << "Failed to parse loaded state." << std::endl;
    } else {
        state_.ReadChatStateMessage(state);
        std::cerr << "Loaded:" << std::endl
                  << state_.users_.size() << " users" << std::endl
                  << state_.topic_channel_map_.size() << " channels" << std::endl;
    }
}

void Server::SubscriberCallback(const TopicMessage& message)
{
    std::lock_guard<std::mutex> lock(lock_);
    if (message.topic_ == TOPIC_USER_ADD) {
        AddUserCallback(message.payload_);
    } else if (message.topic_ == TOPIC_USER_UPDATE) {
        UpdateUserCallback(message.payload_);
    } else if (message.topic_ == TOPIC_CHANNEL_ADD) {
        AddChannelCallback(message.payload_);
    } else if (message.topic_ == TOPIC_CHANNEL_UPDATE) {
        UpdateChannelCallback(message.payload_);
    } else if (message.topic_.find(TOPIC_CHANNEL_PREFIX) != std::string::npos) {
        ChatCallback(message.topic_, message.payload_);
    }else {
        UnknownCallback(message.topic_, message.payload_);
    }
}

chat::Response Server::ServerCallback(const chat::Request &req)
{
    std::lock_guard<std::mutex> lock(lock_);
    chat::Response resp;
    resp.set_id(req.id());
    switch (req.id()) {
    case chat::UNKNOWN_ID:
    {
        break;
    }
    case chat::STATUS:
    {
        resp.set_type(chat::OK);
        break;
    }
    case chat::GET_STATE:
    {
        state_.WriteChatStateMessage(*resp.mutable_chat_state());
        break;
    }
    }
    return resp;
}

void Server::AddUserCallback(const std::string &payload)
{
    std::shared_ptr<chat::User> user(new chat::User);
    user->ParseFromString(payload);
    state_.users_.push_back(user);
    // TODO: handle other cases
}

void Server::UpdateUserCallback(const std::string &payload)
{
    chat::User user_update;
    user_update.ParseFromString(payload);
    for(auto & user : state_.users_){
        if (user->name() == user_update.name()){
            user->set_name(user_update.name());
            break;
        }
    }
}

void Server::AddChannelCallback(const std::string &payload)
{
    std::shared_ptr<chat::Channel> new_channel(new chat::Channel);
    new_channel->ParseFromString(payload);
    state_.topic_channel_map_.insert({ParseChannelTopic(*new_channel), new_channel});
}

void Server::UpdateChannelCallback(const std::string &payload)
{
    chat::Channel new_channel;
    new_channel.ParseFromString(payload);
    //TODO
}

void Server::ChatCallback(const std::string &topic, const std::string &payload)
{
    std::shared_ptr<chat::Message> new_msg(new chat::Message);
    new_msg->ParseFromString(payload);
    auto messages = state_.topic_messages_map_.find(topic);
    if (messages != state_.topic_messages_map_.end()){
        messages->second.push_back(new_msg);
    } else {
        state_.topic_messages_map_.insert({topic, {new_msg}});
    }
}

void Server::UnknownCallback(const std::string &topic, const std::string &payload)
{
    std::cerr << "Uknown message from topic " << topic << "\npayload: " << payload << std::endl;
}
