SET(CMAKE_CXX_FLAGS "-g -Wall -Werror -std=c++11")

## load in pkg-config support
find_package(PkgConfig)
## use pkg-config to get hints for 0mq locations
pkg_check_modules(PC_ZeroMQ QUIET zmq)

## use the hint from above to find where 'zmq.hpp' is located
find_path(ZeroMQ_INCLUDE_DIR
        NAMES zmq.hpp
        PATHS ${PC_ZeroMQ_INCLUDE_DIRS}
        )

## use the hint from about to find the location of libzmq
find_library(ZeroMQ_LIBRARY
        NAMES zmq
        PATHS ${PC_ZeroMQ_LIBRARY_DIRS}
        )

INCLUDE_DIRECTORIES(${CMAKE_CURRENT_BINARY_DIR})
ADD_EXECUTABLE(read read.cpp)
ADD_EXECUTABLE(write write.cpp)
TARGET_LINK_LIBRARIES(read proto ${PROTOBUF_LIBRARY})
TARGET_LINK_LIBRARIES(write proto ${PROTOBUF_LIBRARY})

ADD_LIBRARY(comm SHARED
    publisher.cpp
    subscriber.cpp
    forwarderdevice.cpp
    queuedevice.cpp
    serversocket.cpp
    clientsocket.cpp
    state.cpp
    common.cpp)
TARGET_INCLUDE_DIRECTORIES(comm PUBLIC ${ZeroMQ_INCLUDE_DIR})
TARGET_LINK_LIBRARIES(comm proto ${ZeroMQ_LIBRARY})

#ADD_EXECUTABLE(pub pub.cpp)
#ADD_EXECUTABLE(sub sub.cpp)
#TARGET_INCLUDE_DIRECTORIES(pub PUBLIC ${ZeroMQ_INCLUDE_DIR})
#TARGET_LINK_LIBRARIES(pub proto ${PROTOBUF_LIBRARY} ${ZeroMQ_LIBRARY})
#TARGET_INCLUDE_DIRECTORIES(sub PUBLIC ${ZeroMQ_INCLUDE_DIR})
#TARGET_LINK_LIBRARIES(sub proto ${PROTOBUF_LIBRARY} ${ZeroMQ_LIBRARY})
