import QtQuick 2.3
import QtQuick.Window 2.2
import QtQuick.Controls.Material 2.2
import QtQuick.Controls 2.2

// main window
ApplicationWindow {
    id: idWindow
    visible: true

    width: Screen.desktopAvailableWidth
    height: Screen.desktopAvailableHeight

    MainViewQml{
        id: idMainView
        anchors.fill: parent
    }
}
