#include "chatstateobject.h"

ChatStateObject::ChatStateObject(std::shared_ptr<chat::State> state, QObject *parent)
    : QObject(parent)
    , state_(state)
{

}

std::vector<std::shared_ptr<UserObject>> ChatStateObject::GetUsers() const
{
    std::vector<std::shared_ptr<UserObject>> out;
    for(int i = 0; i < state_->users_size(); ++i){
        out.push_back(std::make_shared<UserObject>(std::make_shared<chat::User>(state_->users(i))));
    }
    return out;
}

std::vector<std::shared_ptr<ChannelObject>> ChatStateObject::GetChannels() const
{
    std::vector<std::shared_ptr<ChannelObject>> out;
    for(int i = 0; i < state_->channels_size(); ++i){
        out.push_back(std::make_shared<ChannelObject>(std::make_shared<chat::Channel>(state_->channels(i))));
    }
    return out;
}

std::vector<std::shared_ptr<MessageObject>> ChatStateObject::GetChannelMessages(QString channel_name) const
{
    std::vector<std::shared_ptr<MessageObject>> out;
    for(int i = 0; i < state_->channels_size(); ++i){
        if (state_->channels(i).name() == channel_name.toStdString()){
            for(int j = 0; j < state_->channels(i).messages_size(); ++j){
                out.push_back(std::make_shared<MessageObject>(std::make_shared<chat::Message>(state_->channels(i).messages(j))));
            }
            break;
        }
    }
    return out;
}

std::shared_ptr<chat::State> ChatStateObject::GetProtoBufObj() const
{
    return state_;
}
