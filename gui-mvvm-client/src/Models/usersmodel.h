#pragma once

#include <QObject>

#include "mainrepository.h"

#include "simplelistmodel.h"
#include "chatstateobject.h"

class UsersModel : public QObject
{
    Q_OBJECT
public:
    explicit UsersModel(std::shared_ptr<MainRepository> main_repo, QObject *parent = nullptr);

    SimpleListModel *GetListModel();
    UserObject *GetCurrentUser();

    void Login(QString name, QString description);
    void NewUser(QString name, QString description);

    void AddUserToList(std::shared_ptr<UserObject> user);

signals:
    void signalCurrentUserChanged();
    void signalUsersListChanged();

public slots:
    void ChatStateUpdate(const std::shared_ptr<ChatStateObject> & state);
    void NewUserUpdate(const std::shared_ptr<UserObject> & user);
    void SendNewUser(std::shared_ptr<UserObject>& new_user);

private:
    std::weak_ptr<MainRepository> main_repo_;
    std::shared_ptr<SimpleListModel> users_list_model_;
};
