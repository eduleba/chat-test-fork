import QtQuick 2.3
import QtQuick.Controls.Material 2.2
import QtQuick.Controls 2.2

Item {
    id: idTopBar

    Row {
        id: idTopBarRow
        anchors.fill: idTopBar
        Item {
            id: idCurrentChannel
            anchors.verticalCenter: idTopBarRow.verticalCenter
            width: (idTopBarRow.width - idButtonSettings.width - 10) / 2
            height: idTopBarRow.height
            Column {
                id: idCurrentChannelCol
                width: idCurrentChannel.width
                Text {
                    id: idCurrentChannelTitle
                    text: qsTr("Current Channel")
                    font.bold: true
                }
                Text {
                    id: idCurrentChannelName
                    text: CommonStore.current_channel !== null ? CommonStore.current_channel.name
                                                                     : ""
                }
                Text {
                    id: idCurrentChannelDescription
                    text: CommonStore.current_channel !== null ? CommonStore.current_channel.description
                                                                     : ""
                    font.italic: true
                }
            }
        }
        Item {
            id: idCurrentUser
            anchors.verticalCenter: idTopBarRow.verticalCenter
            width: (idTopBarRow.width - idButtonSettings.width - 10) / 2
            height: idTopBarRow.height
            Column {
                id: idCurrentUserCol
                width: idCurrentChannel.width
                Text {
                    id: idCurrentUserTitle
                    text: qsTr("Current User")
                    font.bold: true
                }
                Text {
                    id: idCurrentUserName
                    text: CommonStore.current_user !== null ? CommonStore.current_user.name
                                                               : ""
                }
                Text {
                    id: idCurrentUserDescription
                    text: CommonStore.current_user !== null ? CommonStore.current_user.description
                                                               : ""
                    font.italic: true
                }
            }
        }
        Button {
            id: idButtonSettings
            anchors.verticalCenter: idTopBarRow.verticalCenter
            width: 100

            text: qsTr("settings")
            Material.background: Material.Amber
        }
    }
}
