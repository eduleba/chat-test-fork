#pragma once

#include <QObject>

#include "mainrepository.h"

#include "messageobject.h"
#include "chatstateobject.h"
#include "simplelistmodel.h"

class MessagesModel : public QObject
{
    Q_OBJECT

public:
    explicit MessagesModel(std::shared_ptr<MainRepository> main_repo, QObject *parent = nullptr);

    SimpleListModel *GetListModel();

    void AddMessageToList(std::shared_ptr<MessageObject> msg);
    void SetMessages(std::vector<std::shared_ptr<MessageObject>> msg, bool clean = true);
    void CleanMessages();

signals:
    void signalMessagesListChanged();

public slots:
    void ChatStateUpdate(const std::shared_ptr<ChatStateObject> & state);
    void NewMessageUpdate(const std::shared_ptr<MessageObject> & message);
    void SendMessage(QString new_msg);
    void SendMessageObj(std::shared_ptr<MessageObject>& new_msg);
    void ShowCurrentChannelMessages();

private:

    std::weak_ptr<MainRepository> main_repo_;
    std::shared_ptr<SimpleListModel> messages_list_model_;
};

