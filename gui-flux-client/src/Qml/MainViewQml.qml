import QtQuick 2.3
import QtQuick.Controls.Material 2.2
import QtQuick.Controls 2.2

Item {
    id: idMainView

    function openBusyPopup(){
        idBusyPopup.open()
    }

    function closeBusyPopup(){
        idBusyPopup.close()
    }

    BusyPopupQml{
        id: idBusyPopup
        width: idMainView.width / 2
        height: idMainView.height / 2
        anchors.centerIn: idMainView
    }

    LoginPopupQml{
        id: idLoginPopup
        width: idMainView.width / 2
        height: idMainView.height / 2
        anchors.centerIn: idMainView
    }

//    Button{
//        id: button
//        anchors.bottom: idMainView.bottom
//        anchors.right: idMainView.right
//        text: "Test"
//        onClicked: idLoginPopup.open()
//    }

    UsersListQml{
        id: idUsersList
        anchors.left: idMainView.left
        anchors.bottom: idMainView.bottom
        anchors.top: idMainView.verticalCenter
        width: 300
    }

    UserEditQml{
        id: idUserEditPopup
        width: idMainView.width / 2
        height: idMainView.height / 2
        anchors.centerIn: idMainView
    }

    ChannelsListQml{
        id: idChannelsList
        anchors.left: idMainView.left
        anchors.bottom: idMainView.verticalCenter
        anchors.top: idMainView.top
        width: 300
    }

    ChannelEditQml{
        id: idChannelEditPopup
        width: idMainView.width / 2
        height: idMainView.height / 2
        anchors.centerIn: idMainView
    }

    TopBarQml{
        id: idTopBar
        anchors.top: idMainView.top
        anchors.left: idChannelsList.right
        anchors.right: idMainView.right
        anchors.margins: 5
        height: 60
    }

    MessagesViewQml {
        id: idMessagesView
        anchors.top: idTopBar.bottom
        anchors.left: idChannelsList.right
        anchors.right: idMainView.right
        anchors.bottom: idMessagesInsert.top
        anchors.margins: 5
    }

    MessageInsertQml{
        id: idMessagesInsert
        anchors.left: idChannelsList.right
        anchors.right: idMainView.right
        anchors.bottom: idMainView.bottom
        height: 60
    }

    Component.onCompleted: {
        idMainView.openBusyPopup()
    }

}
