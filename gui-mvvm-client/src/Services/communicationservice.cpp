#include "communicationservice.h"

#include <iostream>

CommunicationService::CommunicationService(QObject *parent)
    : QObject(parent)
{

}

void CommunicationService::Start()
{
    std::lock_guard<std::mutex> lock(lock_);
    pub_.StartThread();
    SubscribeBasicTopics();
    sub_.AddCallback(std::bind(&CommunicationService::SubscriberCallback, this, std::placeholders::_1));
    sub_.StartThread();
    std::this_thread::sleep_for(std::chrono::seconds(1));
    emit signalCommunicationStarted();
}

void CommunicationService::Stop()
{
    std::lock_guard<std::mutex> lock(lock_);
    pub_.StopThread();
    sub_.StopThread();
}

const ChatState CommunicationService::GetCurrentState() const
{
    return state_;
}

void CommunicationService::AddChannel(const std::shared_ptr<chat::Channel> &channel)
{
    std::lock_guard<std::mutex> lock(lock_);
    std::string payload;
    if(channel->SerializeToString(&payload)){
        pub_.Publish(TopicMessage(TOPIC_CHANNEL_ADD, payload));
    }
}

void CommunicationService::AddUser(const std::shared_ptr<chat::User> &user)
{
    std::lock_guard<std::mutex> lock(lock_);
    std::string payload;
    if(user->SerializeToString(&payload)){
        pub_.Publish(TopicMessage(TOPIC_USER_ADD, payload));
    }
}

void CommunicationService::SendMessage(const std::shared_ptr<chat::Channel>& channel,
                                          const std::shared_ptr<chat::Message> &message)
{
    std::string payload;
    if(message->SerializeToString(&payload)){
        std::string topic = ParseChannelTopic(*channel);
        pub_.Publish(TopicMessage(topic, payload));
//        ChatCallback(topic, payload);
    }
}

void CommunicationService::FetchServerState()
{
    chat::Request req;
    req.set_id(chat::GET_STATE);
    auto rep = client_.SendRequest(req);
    if (rep && rep->has_chat_state()){
        std::shared_ptr<chat::State> state(new chat::State);
        *state = rep->chat_state();
        state_.ReadChatStateMessage(*state);
        std::cout << "Synced users: " << state_.users_.size() << std::endl;
        std::cout << "Synced channels: " << state_.topic_channel_map_.size() << std::endl;
        emit signalNewServerState(std::make_shared<ChatStateObject>(state));
    } else {
        std::cerr << "FetchServerState failed!" << std::endl;
    }
}

void CommunicationService::SubscribeBasicTopics()
{
    // TODO: select smaller number of topics
    sub_.SubscribeAll();
}

void CommunicationService::SubscriberCallback(const TopicMessage &message)
{
    std::lock_guard<std::mutex> lock(lock_);
    if (message.topic_ == TOPIC_USER_ADD) {
        AddUserCallback(message.payload_);
    } else if (message.topic_ == TOPIC_USER_UPDATE) {
        UpdateUserCallback(message.payload_);
    } else if (message.topic_ == TOPIC_CHANNEL_ADD) {
        AddChannelCallback(message.payload_);
    } else if (message.topic_ == TOPIC_CHANNEL_UPDATE) {
        UpdateChannelCallback(message.payload_);
    } else if (message.topic_.find(TOPIC_CHANNEL_PREFIX) != std::string::npos) {
        ChatCallback(message.topic_, message.payload_);
    }else {
        UnknownCallback(message.topic_, message.payload_);
    }
}

void CommunicationService::AddUserCallback(const std::string &payload)
{
    std::shared_ptr<chat::User> user(new chat::User);
    if (user->ParseFromString(payload)){
        state_.users_.push_back(user);
        emit signalNewUser(std::make_shared<UserObject>(user));
    }
}

void CommunicationService::UpdateUserCallback(const std::string &payload)
{
    chat::User user_update;
    if (user_update.ParseFromString(payload)){
        for(auto & user : state_.users_){
            if (user->name() == user_update.name()){
                user->set_name(user_update.name());
                break;
            }
        }
    }
}

void CommunicationService::AddChannelCallback(const std::string &payload)
{
    std::shared_ptr<chat::Channel> new_channel(new chat::Channel);
    if (new_channel->ParseFromString(payload)){
        state_.topic_channel_map_.insert({ParseChannelTopic(*new_channel), new_channel});
        emit signalNewChannel(std::make_shared<ChannelObject>(new_channel));
    }
}

void CommunicationService::UpdateChannelCallback(const std::string &payload)
{
    chat::Channel new_channel;
    if (new_channel.ParseFromString(payload)){}
    //TODO
}

void CommunicationService::ChatCallback(const std::string &topic, const std::string &payload)
{
    std::shared_ptr<chat::Message> new_msg(new chat::Message);
    if (new_msg->ParseFromString(payload)){
        auto messages = state_.topic_messages_map_.find(topic);
        if (messages != state_.topic_messages_map_.end()){
            messages->second.push_back(new_msg);
        } else {
            state_.topic_messages_map_.insert({topic, {new_msg}});
        }
        emit signalNewMessage(std::make_shared<MessageObject>(new_msg));
    }
}

void CommunicationService::UnknownCallback(const std::string &topic, const std::string &payload)
{
    std::cerr << "Uknown message from topic " << topic << "\npayload: " << payload << std::endl;
}
