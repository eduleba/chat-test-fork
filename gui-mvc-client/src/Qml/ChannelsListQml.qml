import QtQuick 2.3
import QtQuick.Controls.Material 2.2
import QtQuick.Controls 2.2

import Views 1.0

ChannelsList {
    id: idChannelList

    signal signalAddChannel()

    ListView{
        id: idChannelsList
        model: channels_model
        anchors.fill: idChannelList
        clip: true
        header: Text{
            text: "Channels"
        }

        delegate:  SwipeDelegate {
            id: swipeDelegate
            text: model.name + " - " + model.description
            width: parent.width

            onClicked: {idChannelList.SelectChannel(model.name)}
//            height: 30

            ListView.onRemove: SequentialAnimation {
                PropertyAction {
                    target: swipeDelegate
                    property: "ListView.delayRemove"
                    value: true
                }
                NumberAnimation {
                    target: swipeDelegate
                    property: "height"
                    to: 0
                    easing.type: Easing.InOutQuad
                }
                PropertyAction {
                    target: swipeDelegate
                    property: "ListView.delayRemove"
                    value: false
                }
            }


            swipe.right: Label {
                id: deleteLabel
                text: qsTr("Delete")
                color: "white"
                verticalAlignment: Label.AlignVCenter
                padding: 12
                height: parent.height
                anchors.right: parent.right

                SwipeDelegate.onClicked: idChannelsList.model.remove(index)

                background: Rectangle {
                    color: deleteLabel.SwipeDelegate.pressed ? Qt.darker("tomato", 1.1) : "tomato"
                }
            }
        }
    }
    RoundButton {
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.margins: 5

        Material.background: Material.Amber

        text: "+" // Unicode Character 'CHECK MARK'
        onClicked: idChannelList.signalAddChannel()
    }

}
