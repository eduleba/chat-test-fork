#include "clientsocket.h"

#include <iostream>

ClientSocket::ClientSocket(bool bind)
    : bind_socket_(bind), address_(TCP_SOCKET_REQ)
{
    SetupZMQ();
}

ClientSocket::ClientSocket(const std::string &address, bool bind)
    : bind_socket_(bind), address_(address)
{
    SetupZMQ();
}

ClientSocket::~ClientSocket()
{

}

std::shared_ptr<chat::Response> ClientSocket::SendRequest(const chat::Request& req)
{
    std::shared_ptr<chat::Response> rep(nullptr);
    std::string payload;
    if(req.SerializeToString(&payload)){
        auto size = zmq_client_->send(payload.c_str(), payload.size());
        if (size != payload.size()){
            // log issue
            std::cerr << "Sending Request Failed! " << size << std::endl;
            return rep;
        } else {
            zmq::message_t rep_msg;
            int checks = timeout_ms_ / interval_ms_;
            bool received = false;
            while (checks > 0) {
                received = zmq_client_->recv(&rep_msg, ZMQ_NOBLOCK);
                if (received) break;
                checks--;
                std::this_thread::sleep_for(std::chrono::milliseconds(interval_ms_));
            }
            if (received){
                auto rep_str = std::string(static_cast<char*>(rep_msg.data()), rep_msg.size());
                rep.reset(new chat::Response());
                rep->ParseFromString(rep_str);
            } else {
                std::cerr << "Waiting for request timeout!" << std::endl;
            }
        }
    }
    return rep;
}

void ClientSocket::SetupZMQ()
{
    context_.reset(new zmq::context_t(1));
    zmq_client_.reset(new zmq::socket_t(*context_, ZMQ_REQ));
    if (bind_socket_){
        zmq_client_->bind(address_);
    } else {
        zmq_client_->connect(address_);
    }
    std::this_thread::sleep_for(std::chrono::seconds(1));
}
