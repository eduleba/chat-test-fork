#include "dispatcher.h"

Dispatcher::Dispatcher(QObject *parent) : QObject(parent)
{

}

void Dispatcher::DispatchAction(int id)
{
    DispatchAction(id, {});
}

void Dispatcher::DispatchAction(int id, QVariantMap payload)
{
    for(auto & callback : callbacks_){
        callback(static_cast<Actions::ACTION_ID>(id), payload);
    }
}

void Dispatcher::AddCallback(std::function<void (const Actions::ACTION_ID &, const QVariantMap &)> callback)
{
    callbacks_.push_back(callback);
}

void Dispatcher::DispatchStartAppAction()
{
    DispatchAction(Actions::START_APP);
}
