#include <zmq.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

#include "proto/chat.pb.h"
using namespace std;

// This function fills in a Person message based on user input.
void PromptForAddress(tutorial::Person* person) {
  cout << "Enter person ID number: ";
  int id;
  cin >> id;
  person->set_id(id);
  cin.ignore(256, '\n');

  cout << "Enter name: ";
  getline(cin, *person->mutable_name());

  cout << "Enter email address (blank for none): ";
  string email;
  getline(cin, email);
  if (!email.empty()) {
    person->set_email(email);
  }

  while (true) {
    cout << "Enter a phone number (or leave blank to finish): ";
    string number;
    getline(cin, number);
    if (number.empty()) {
      break;
    }

    tutorial::Person::PhoneNumber* phone_number = person->add_phones();
    phone_number->set_number(number);

    cout << "Is this a mobile, home, or work phone? ";
    string type;
    getline(cin, type);
    if (type == "mobile") {
      phone_number->set_type(tutorial::Person::MOBILE);
    } else if (type == "home") {
      phone_number->set_type(tutorial::Person::HOME);
    } else if (type == "work") {
      phone_number->set_type(tutorial::Person::WORK);
    } else {
      cout << "Unknown phone type.  Using default." << endl;
    }
  }
}

int main () {
    GOOGLE_PROTOBUF_VERIFY_VERSION;

    //  Prepare our context and publisher
    zmq::context_t context (1);
    zmq::socket_t publisher (context, ZMQ_PUB);
//    publisher.bind("tcp://*:5556");
    publisher.bind("ipc://test.ipc");

    while (1) {

        tutorial::AddressBook address_book;

        // Add an address.
        PromptForAddress(address_book.add_people());

          // Write the new address book back to disk.
           std::ostringstream output;
          if (!address_book.SerializeToOstream(&output)) {
            cerr << "Failed to write address book." << endl;
            continue;
          }
        //  Send message to all subscribers
        zmq::message_t message(output.str().c_str(), output.str().size());
        publisher.send(message);

    }
    google::protobuf::ShutdownProtobufLibrary();
    return 0;
}
