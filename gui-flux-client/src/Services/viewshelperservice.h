#pragma once

#include <QObject>

#include "action.h"

class ViewsHelper : public QObject
{
    Q_OBJECT
public:
    explicit ViewsHelper(QObject *parent = nullptr);

    void HandleAction(const Actions::ACTION_ID & action, const QVariantMap & payload);

signals:
    void signalShowBusyPopup();
    void signalHideBusyPopup();
    void signalShowLoginPopup();
    void signalHideLoginPopup();
    void signalOpenChannelPopup();
    void signalOpenUserPopup();

public slots:
};
