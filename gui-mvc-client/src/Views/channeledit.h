#pragma once

#include <memory>

#include <QQuickItem>

#include "channelobject.h"

class ChannelEdit : public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(QObject* channel READ GetEditedChannel NOTIFY signalChannelChanged)

public:
    ChannelEdit();
    QObject *GetEditedChannel() const;

signals:
    void signalChannelChanged(QObject* channel);
    void signalEditedChannel(QString, QString);

public slots:
    void SetChannel(std::shared_ptr<ChannelObject> channel);

private:
    std::shared_ptr<ChannelObject> channel_;
};
