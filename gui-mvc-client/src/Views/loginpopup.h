#pragma once

#include <QQuickItem>

class LoginPopup : public QQuickItem
{
    Q_OBJECT
public:
    LoginPopup();

    void Show();
    void Hide();

signals:
    void signalLogin(QString, QString);

public slots:
};
