#pragma once

#include <memory>

#include <QObject>

#include "messagesmodel.h"

class MessagesViewModel : public QObject
{
    Q_OBJECT
    Q_PROPERTY(SimpleListModel* messages_list_model READ GetModel NOTIFY signalModelChanged)
public:
    explicit MessagesViewModel(std::shared_ptr<MessagesModel> messages_model_, QObject *parent = nullptr);

    SimpleListModel *GetModel() const;

    Q_INVOKABLE void SendMessage(QString message);

signals:
    void signalModelChanged(SimpleListModel* messages_model);

public slots:

private:
    std::shared_ptr<MessagesModel> model_;
};
