#include "commonstore.h"

#include "communicationservice.h"
#include "viewshelperservice.h"

#include "dispatcher.h"

#include "channelsstore.h"
#include "messagesstore.h"
#include "usersstore.h"

CommonStore::CommonStore(std::shared_ptr<MainRepository> main_repo, QObject *parent)
    : QObject(parent)
    , main_repo_(main_repo)
    , current_channel_(nullptr)
    , current_user_(nullptr)
{

}

void CommonStore::HandleAction(const Actions::ACTION_ID &action, const QVariantMap &payload)
{
    if (action == Actions::START_APP){
        StartApp();
    } else if (action == Actions::START_LOGIN) {
        ShowLoginPopup();
    } else if (action == Actions::LOGIN_USER) {
        Login(payload["name"].toString(), payload["description"].toString());
        MainRepository::GetDispatcher(main_repo_)->DispatchAction(Actions::NEW_USER, payload);
    } else if (action == Actions::SELECT_CHANNEL) {
        SelectChannel(payload["name"].toString());
    }
}


std::shared_ptr<ChannelObject> CommonStore::GetCurrentChannel()
{
    return current_channel_;
}

ChannelObject *CommonStore::GetCurrentChannelRaw()
{
    return current_channel_.get();
}

std::shared_ptr<UserObject> CommonStore::GetCurrentUser()
{
    return current_user_;
}

UserObject *CommonStore::GetCurrentUserRaw()
{
    return  current_user_.get();
}

void CommonStore::CurrentUserUpdate(std::shared_ptr<UserObject> &user)
{
    SetUser(user);
}

void CommonStore::CurrentChannelUpdate(std::shared_ptr<ChannelObject> &channel)
{
    SetChannel(channel);
}

void CommonStore::SetChannel(std::shared_ptr<ChannelObject> &channel)
{
    current_channel_ = channel;
    emit signalCurrentChannelChanged();
}

void CommonStore::SetUser(std::shared_ptr<UserObject> &user)
{
    current_user_ = user;
    emit signalCurrentUserChanged();
}

void CommonStore::ShowLoginPopup()
{
    auto helper = MainRepository::GetViewsHelperService(main_repo_);
    emit helper->signalHideBusyPopup();
    emit helper->signalShowLoginPopup();
}

void CommonStore::SelectChannel(QString name)
{
    auto & state = MainRepository::GetCommunicationService(main_repo_)->GetCurrentState();
    for(auto & channel : state.topic_channel_map_){
        if (channel.second->name() == name.toStdString()){
            auto channel_obj = std::make_shared<ChannelObject>(channel.second);
            SetChannel(channel_obj);
            MainRepository::GetMessagesStore(main_repo_)->ShowCurrentChannelMessages();
            break;
        }
    }
}

void CommonStore::StartApp()
{
    communication_start_ = std::async(std::launch::async, [this]{
        auto comm_service =  MainRepository::GetCommunicationService(main_repo_);
        comm_service->Start();
        comm_service->FetchServerState();});
}

void CommonStore::Login(QString name, QString description)
{
    std::shared_ptr<chat::User> user_proto(new chat::User);
    user_proto->set_name(name.toStdString());
    user_proto->set_description(description.toStdString());
    auto user_obj = std::make_shared<UserObject>(user_proto);
    SetUser(user_obj);
}
