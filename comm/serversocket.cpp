#include "serversocket.h"

#include <iostream>
#include <sstream>

ServerSocket::ServerSocket(bool bind /*= false*/)
 : bind_socket_(bind), address_(TCP_SOCKET_REP), loop_running_(false)
{
    SetupZMQ();
}

ServerSocket::ServerSocket(const std::string &address, bool bind /*= false*/)
 : bind_socket_(bind), address_(address), loop_running_(false)
{
    SetupZMQ();
}

ServerSocket::~ServerSocket()
{
    StopThread();
}

void ServerSocket::StartThread()
{
    if (loop_running_) return;
    if (context_ == nullptr){
        SetupZMQ();
    }
    thread_ = std::thread(&ServerSocket::ServerLoop, this);
    loop_running_ = true;
    std::this_thread::sleep_for(std::chrono::seconds(1));
}

void ServerSocket::SetupZMQ()
{
    context_.reset(new zmq::context_t(1));
    zmq_server_.reset(new zmq::socket_t(*context_, ZMQ_REP));
    if (bind_socket_){
        zmq_server_->bind(address_);
    } else {
        zmq_server_->connect(address_);
    }
}

void ServerSocket::AddCallback(std::function<chat::Response(const chat::Request&)> callback)
{
    callback_ = callback;
}

void ServerSocket::StopThread()
{
    if (!loop_running_) return;
    loop_running_ = false;
    Stop();
    if (thread_.joinable()){
        thread_.join();
    }
}

std::string ServerSocket::CallbackWrapper(const std::string &request)
{
    chat::Response rep_error;
    rep_error.set_id(chat::UNKNOWN_ID);
    rep_error.set_type(chat::ERROR);

    chat::Request req;
    if (req.ParseFromString(request)){
        if (!callback_){
            rep_error.set_id(req.id());
            return rep_error.SerializeAsString();
        }
        chat::Response rep = callback_(req);
        return rep.SerializeAsString();
    }
    return rep_error.SerializeAsString();
}

void ServerSocket::Stop()
{
    zmq_server_.reset(nullptr);
    context_.reset(nullptr);
}

void ServerSocket::ServerLoop()
{
    try {
        while (loop_running_) {
            zmq::message_t mgs;
            zmq_server_->recv(&mgs);
            auto msg_str = std::string(static_cast<char*>(mgs.data()), mgs.size());
            std::cout << "Request: " << msg_str << std::endl;

            auto rep_msg_str = CallbackWrapper(msg_str);

            auto size = rep_msg_str.size();
            auto payload_size = zmq_server_->send(rep_msg_str.c_str(), size);
            if (size != payload_size){
                // log issue
                std::cerr << "E1" << std::endl;
                continue;
            } else {
                std::cerr << "Response: " << rep_msg_str << std::endl;
            }
        }
    } catch (zmq::error_t & e) {
        std::cerr << "ServerSocket Thread: " << e.what() << std::endl;
    }
}
