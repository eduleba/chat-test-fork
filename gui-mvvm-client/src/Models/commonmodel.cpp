#include "commonmodel.h"

#include "communicationservice.h"
#include "viewshelperservice.h"

#include "channelsmodel.h"
#include "messagesmodel.h"
#include "usersmodel.h"

CommonModel::CommonModel(std::shared_ptr<MainRepository> main_repo, QObject *parent)
    : QObject(parent)
    , main_repo_(main_repo)
    , current_channel_(nullptr)
    , current_user_(nullptr)
{
    // TODO: move to better place
    QObject::connect(MainRepository::GetCommunicationService(main_repo_).get(), &CommunicationService::signalCommunicationStarted,
                     this, &CommonModel::ShowLoginPopup, Qt::QueuedConnection);

}


std::shared_ptr<ChannelObject> CommonModel::GetCurrentChannel()
{
    return current_channel_;
}

std::shared_ptr<UserObject> CommonModel::GetCurrentUser()
{
    return current_user_;
}

void CommonModel::CurrentUserUpdate(std::shared_ptr<UserObject> &user)
{
    SetUser(user);
}

void CommonModel::CurrentChannelUpdate(std::shared_ptr<ChannelObject> &channel)
{
    SetChannel(channel);
}

void CommonModel::SetChannel(std::shared_ptr<ChannelObject> &channel)
{
    current_channel_ = channel;
    emit MainRepository::GetChannelsModel(main_repo_)->signalCurrentChannelChanged();
}

void CommonModel::SetUser(std::shared_ptr<UserObject> &user)
{
    current_user_ = user;
    emit MainRepository::GetUsersModel(main_repo_)->signalCurrentUserChanged();
}

void CommonModel::ShowLoginPopup()
{
    auto helper = MainRepository::GetViewsHelperService(main_repo_);
    emit helper->signalHideBusyPopup();
    emit helper->signalShowLoginPopup();
}

void CommonModel::LoginUser(std::shared_ptr<UserObject> &user)
{
    MainRepository::GetUsersModel(main_repo_)->SendNewUser(user);
    SetUser(user);
}

void CommonModel::SelectChannel(QString name)
{
    auto & state = MainRepository::GetCommunicationService(main_repo_)->GetCurrentState();
    for(auto & channel : state.topic_channel_map_){
        if (channel.second->name() == name.toStdString()){
            auto channel_obj = std::make_shared<ChannelObject>(channel.second);
            SetChannel(channel_obj);
            MainRepository::GetMessagesModel(main_repo_)->ShowCurrentChannelMessages();
            break;
        }
    }
}

void CommonModel::StartApp()
{
    communication_start_ = std::async(std::launch::async, [this]{
        auto comm_service =  MainRepository::GetCommunicationService(main_repo_);
        comm_service->Start();
        comm_service->FetchServerState();});
}
