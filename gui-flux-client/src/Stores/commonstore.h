#pragma once

#include <future>
#include <memory>

#include <QObject>

#include "action.h"
#include "mainrepository.h"

#include "channelobject.h"
#include "userobject.h"

class CommonStore : public QObject
{
    Q_OBJECT
    Q_PROPERTY(UserObject* current_user READ GetCurrentUserRaw NOTIFY signalCurrentUserChanged)
    Q_PROPERTY(ChannelObject* current_channel READ GetCurrentChannelRaw NOTIFY signalCurrentChannelChanged)
public:
    explicit CommonStore(std::shared_ptr<MainRepository> main_repo, QObject *parent = nullptr);

    void HandleAction(const Actions::ACTION_ID & action, const QVariantMap & payload);

    std::shared_ptr<ChannelObject> GetCurrentChannel();
    ChannelObject *GetCurrentChannelRaw();

    std::shared_ptr<UserObject> GetCurrentUser();
    UserObject *GetCurrentUserRaw();

signals:
    void signalCurrentChannelChanged();
    void signalCurrentUserChanged();

public slots:
    void CurrentUserUpdate(std::shared_ptr<UserObject> & user);
    void SetUser(std::shared_ptr<UserObject>& user);

    void CurrentChannelUpdate(std::shared_ptr<ChannelObject> & channel);
    void SetChannel(std::shared_ptr<ChannelObject>& channel);

    void ShowLoginPopup();
    void Login(QString name, QString description);

    void SelectChannel(QString name);

    void StartApp();

private:
    std::weak_ptr<MainRepository> main_repo_;
    std::shared_ptr<ChannelObject> current_channel_;
    std::shared_ptr<UserObject> current_user_;
    std::future<void> communication_start_;
};
