#pragma once

#include <atomic>
#include <functional>
#include <thread>

#include <zmq.hpp>

#include "common.h"

class Subscriber
{
public:
    Subscriber(bool bind = false);
    Subscriber(const std::string& address, bool bind = false);
    ~Subscriber();

    void StartThread();
    void AddCallback(std::function<void(const TopicMessage&)> callback);
    void StopThread();

    void SubscribeTopic(const std::string& topic);
    void SubscribeAll();
    void UnsubscribeTopic(const std::string& topic);
    void UnsubscribeAll();

private:
    void SetupZMQ();
    void SubStopMessage();
    void SubLoop();

    bool bind_socket_;
    std::string address_;
    std::unique_ptr<zmq::context_t> context_;
    std::unique_ptr<zmq::socket_t> zmq_subscriber_;
    std::atomic<bool> loop_running_;
    std::thread thread_;
    std::function<void(const TopicMessage&)> callback_;
};
