#include <iostream>
#include <sstream>
#include <string>

#include <zmq.hpp>

#include "proto/chat.pb.h"

#include "subscriber.h"

using namespace std;

int main ()
{

    GOOGLE_PROTOBUF_VERIFY_VERSION;

    Subscriber sub;
    sub.SubscribeAll();
    sub.StartThread();

    std::string msg;
    std::cin >> msg;

    sub.StopThread();

    google::protobuf::ShutdownProtobufLibrary();
    return 0;
}
