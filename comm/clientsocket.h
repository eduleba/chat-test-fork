#pragma once
#include <atomic>
#include <functional>
#include <thread>

#include <zmq.hpp>

#include "common.h"
#include "proto/chat.pb.h"

class ClientSocket
{
public:
    ClientSocket(bool bind = false);
    ClientSocket(const std::string& address, bool bind = false);
    ~ClientSocket();

    const int timeout_ms_ = 5000;
    const int interval_ms_ = 100;


    std::shared_ptr<chat::Response> SendRequest(const chat::Request& req);

private:
    void SetupZMQ();

    bool bind_socket_;
    std::string address_;
    std::unique_ptr<zmq::context_t> context_;
    std::unique_ptr<zmq::socket_t> zmq_client_;
};
