#include "client.h"

#include <iostream>
#include <fstream>

Client::Client()
    : expectig_setup_(false)
{

}

void Client::Start()
{
    pub_.StartThread();
    SubscribeBasicTopics();
    sub_.AddCallback(std::bind(&Client::SubscriberCallback, this, std::placeholders::_1));
    sub_.StartThread();
    std::this_thread::sleep_for(std::chrono::seconds(1));
}

void Client::Stop()
{
    pub_.StopThread();
    sub_.StopThread();
}

void Client::Save(const std::string &file)
{
    std::lock_guard<std::mutex> lock(lock_);
    std::fstream output(file, std::ios::out | std::ios::trunc | std::ios::binary);
    if (!current_user.SerializeToOstream(&output)) {
        std::cerr << "Failed to save state." << std::endl;
    }
}

void Client::Load(const std::string &file)
{
    std::lock_guard<std::mutex> lock(lock_);
    chat::User user;
    std::fstream input(file, std::ios::in | std::ios::binary);
    if (!input) {
        std::cerr << "Invalid file: " << file << std::endl;
    } else if (!user.ParseFromIstream(&input)) {
        std::cerr << "Failed to parse loaded state." << std::endl;
    } else {
        current_user = user;
    }
}

void Client::ListUsers()
{
    std::lock_guard<std::mutex> lock(lock_);
    for (auto & user : state_.users_){
        std::cout << (user->name() == current_user.name() ? "*" : " ")
                  << user->name()
                  << (user->name() == current_user.name() ? "*" : " ")
                  << std::endl;
    }
}

void Client::ListChannels()
{
    std::lock_guard<std::mutex> lock(lock_);
    for (auto & channel : state_.topic_channel_map_){
        std::cout << (channel.second->name() == current_channel.name() ? "*" : " ")
                  << channel.second->name()
                  << (channel.second->name() == current_channel.name() ? "*" : " ")
                  << std::endl;
    }
}

void Client::ChangeChannel(const std::string &name)
{
    std::lock_guard<std::mutex> lock(lock_);
    for(auto & topic_channel : state_.topic_channel_map_){
        if (topic_channel.second->name() == name){
            current_channel = *topic_channel.second;
            ShowCurrentChannelMessages();
            return;
        }
    }
    std::cout << "No such channel!" << std::endl;
}

void Client::AddChannel(const chat::Channel &channel)
{
    std::lock_guard<std::mutex> lock(lock_);
    current_channel = channel;
    std::string payload;
    if(channel.SerializeToString(&payload)){
        pub_.Publish(TopicMessage(TOPIC_CHANNEL_ADD, payload));
//        AddChannelCallback(payload);
    }
}

void Client::AddUser(const chat::User &user)
{
    std::lock_guard<std::mutex> lock(lock_);
    current_user = user;
    std::string payload;
    if(user.SerializeToString(&payload)){
        pub_.Publish(TopicMessage(TOPIC_USER_ADD, payload));
//        AddUserCallback(payload);
    }
}

void Client::SendMessage(const std::string &message)
{
    chat::Message msg;
    msg.set_ms_since_epoch_utc(1); // TODO: fixme
    msg.set_author(current_user.name());
    msg.set_content(message);
    SendMessage(msg);
}

void Client::SendMessage(const chat::Message &message)
{
    std::string payload;
    if(message.SerializeToString(&payload)){
        std::string topic = ParseChannelTopic(current_channel);
        pub_.Publish(TopicMessage(topic, payload));
        ChatCallback(topic, payload);
    }
}

void Client::FetchServerState()
{
    chat::Request req;
    req.set_id(chat::GET_STATE);
    auto rep = client_.SendRequest(req);
    if (rep && rep->has_chat_state()){
        std::shared_ptr<chat::State> state(new chat::State);
        *state = rep->chat_state();
        state_.ReadChatStateMessage(*state);
        std::cout << "Synced users: " << state_.users_.size() << std::endl;
        std::cout << "Synced channels: " << state_.topic_channel_map_.size() << std::endl;
    } else {
        std::cerr << "FetchServerState failed!" << std::endl;
    }
}

void Client::ShowCurrentChannelMessages()
{
    auto channel = state_.topic_messages_map_.find(ParseChannelTopic(current_channel));
    if(channel != state_.topic_messages_map_.end() && !channel->second.empty()){
        for(auto & msg : channel->second){
            std::cerr << msg->author() << ": " <<  msg->content() << std::endl;
        }
    } else {
        std::cout << "No messages in channel!" << std::endl;
    }
}

void Client::SubscribeBasicTopics()
{
    // TODO: select smaller number of topics
    sub_.SubscribeAll();
}

void Client::SubscriberCallback(const TopicMessage &message)
{
    std::lock_guard<std::mutex> lock(lock_);
    if (message.topic_ == TOPIC_USER_ADD) {
        AddUserCallback(message.payload_);
    } else if (message.topic_ == TOPIC_USER_UPDATE) {
        UpdateUserCallback(message.payload_);
    } else if (message.topic_ == TOPIC_CHANNEL_ADD) {
        AddChannelCallback(message.payload_);
    } else if (message.topic_ == TOPIC_CHANNEL_UPDATE) {
        UpdateChannelCallback(message.payload_);
    } else if (message.topic_.find(TOPIC_CHANNEL_PREFIX) != std::string::npos) {
        ChatCallback(message.topic_, message.payload_);
    }else {
        UnknownCallback(message.topic_, message.payload_);
    }
}

void Client::AddUserCallback(const std::string &payload)
{
    std::shared_ptr<chat::User> user(new chat::User);
    if (user->ParseFromString(payload)){
        state_.users_.push_back(user);
    }
}

void Client::UpdateUserCallback(const std::string &payload)
{
    chat::User user_update;
    if (user_update.ParseFromString(payload)){
        for(auto & user : state_.users_){
            if (user->name() == user_update.name()){
                user->set_name(user_update.name());
                break;
            }
        }
    }
}

void Client::AddChannelCallback(const std::string &payload)
{
    std::shared_ptr<chat::Channel> new_channel(new chat::Channel);
    if (new_channel->ParseFromString(payload)){
        state_.topic_channel_map_.insert({ParseChannelTopic(*new_channel), new_channel});
    }
}

void Client::UpdateChannelCallback(const std::string &payload)
{
    chat::Channel new_channel;
    if (new_channel.ParseFromString(payload)){}
    //TODO
}

void Client::ChatCallback(const std::string &topic, const std::string &payload)
{
    std::shared_ptr<chat::Message> new_msg(new chat::Message);
    if (new_msg->ParseFromString(payload)){
        auto messages = state_.topic_messages_map_.find(topic);
        if (messages != state_.topic_messages_map_.end()){
            messages->second.push_back(new_msg);
        } else {
            state_.topic_messages_map_.insert({topic, {new_msg}});
        }
        std::string current_channel_topic = ParseChannelTopic(current_channel);
        if (new_msg->author() != current_user.name() && current_channel_topic == topic){
            std::cerr << new_msg->author() << ": " <<  new_msg->content() << std::endl;
        }
    }
}

void Client::UnknownCallback(const std::string &topic, const std::string &payload)
{
    std::cerr << "Uknown message from topic " << topic << "\npayload: " << payload << std::endl;
}
