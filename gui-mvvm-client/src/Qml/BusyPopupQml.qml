import QtQuick 2.3
import QtQuick.Window 2.2
import QtQuick.Controls.Material 2.2
import QtQuick.Controls 2.2

Item {
    id: idBusyPopup

    Component.onCompleted: {
        ViewsHelper.signalShowBusyPopup.connect(idBusyPopup.open)
        ViewsHelper.signalHideBusyPopup.connect(idBusyPopup.close)
    }

    function open(){
        idPopup.open()
    }

    function close(){
        idPopup.close()
    }

    Popup {
        id: idPopup
        width: idBusyPopup.width
        height: idBusyPopup.height
        modal: true
        focus: true
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent

        onClosed: idIndicator.running = false
        onOpened: idIndicator.running = true

        contentItem: Item {
                id: idCol
                anchors.fill: parent
                BusyIndicator{
                    id: idIndicator
                    anchors.centerIn: idCol
                    running: false
                    Material.accent: Material.Amber
                }
                Text {
                    id: idContent
                    text: "Please wait..."
                    anchors.horizontalCenter: idIndicator.horizontalCenter
                    anchors.bottom: idCol.bottom
                    anchors.bottomMargin: 20
                }
            }
    }

}
