#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickStyle>
#include <QTimer>

#include "mainview.h"
#include "customlistmodel.h"

#include "Controllers/logiccontroller.h"
#include "Controllers/viewscontroller.h"
#include "Controllers/communicationcontroller.h"

void RegisterComponents(){
    qRegisterMetaType<std::shared_ptr<MessageObject>>("std::shared_ptr<MessageObject>");
    qRegisterMetaType<std::shared_ptr<UserObject>>("std::shared_ptr<UserObject>");
    qRegisterMetaType<std::shared_ptr<ChannelObject>>("std::shared_ptr<ChannelObject>");
    qRegisterMetaType<std::shared_ptr<ChatStateObject>>("std::shared_ptr<ChatStateObject>");

//    qmlRegisterInterface<Interface>("Interface");

    qmlRegisterType<MainView>("Views", 1, 0, "MainView");
    qmlRegisterType<ChannelEdit>("Views", 1, 0, "ChannelEdit");
    qmlRegisterType<TopBar>("Views", 1, 0, "TopBar");
    qmlRegisterType<ChannelsList>("Views", 1, 0, "ChannelsList");
    qmlRegisterType<MessagesInsert>("Views", 1, 0, "MessagesInsert");
    qmlRegisterType<UserEdit>("Views", 1, 0, "UserEdit");
    qmlRegisterType<LoginPopup>("Views", 1, 0, "LoginPopup");
    qmlRegisterType<MessagesView>("Views", 1, 0, "MessagesView");
    qmlRegisterType<UsersList>("Views", 1, 0, "UsersList");

    qmlRegisterType<CustomListModel>("Views", 1, 0, "CustomListModel");
}

int main(int argc, char *argv[])
{
    QQuickStyle::setStyle("Material");
    QCoreApplication::addLibraryPath("./");

    QGuiApplication app(argc, argv);

    RegisterComponents();

    auto logic_controller = std::make_shared<LogicController>();
    auto views_controller = std::make_shared<ViewsController>(logic_controller);
    auto comm_controller = std::make_shared<CommunicationController>(logic_controller);

    logic_controller->SetCommunicationController(comm_controller);
    logic_controller->SetViewsController(views_controller);
    logic_controller->ConnectControllers();

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    auto root_objects = engine.rootObjects();
    if (root_objects.empty()) return 1;
    views_controller->SetMainView(root_objects.first());

    QTimer::singleShot(0, logic_controller.get(), &LogicController::StartApp);

    return app.exec();
}

