#pragma once

#include <QObject>

class ViewsHelper : public QObject
{
    Q_OBJECT
public:
    explicit ViewsHelper(QObject *parent = nullptr);

signals:
    void signalShowBusyPopup();
    void signalHideBusyPopup();
    void signalShowLoginPopup();
    void signalHideLoginPopup();

public slots:
};
