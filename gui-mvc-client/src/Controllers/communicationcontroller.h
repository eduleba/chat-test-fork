#pragma once

#include <atomic>
#include <map>
#include <mutex>
#include <vector>

#include "subscriber.h"
#include "publisher.h"
#include "clientsocket.h"
#include "state.h"
#include "proto/chat.pb.h"

#include <QObject>

#include "chatstateobject.h"
#include "logiccontroller.h"

class CommunicationController : public QObject
{
    Q_OBJECT
public:
    explicit CommunicationController(std::shared_ptr<LogicController> logic_controller,
                                     QObject *parent = nullptr);
    virtual ~CommunicationController(){}

    void Start();
    void Stop();

    const ChatState GetCurrentState() const;

    void AddChannel(const std::shared_ptr<chat::Channel>& channel);
    void AddUser(const std::shared_ptr<chat::User>& user);
    void SendMessage(const std::shared_ptr<chat::Channel>& channel,
                     const std::shared_ptr<chat::Message>& message);

    void FetchServerState();

signals:
    void signalNewServerState(const std::shared_ptr<ChatStateObject> &);
    void signalNewUser(const std::shared_ptr<UserObject> &);
    void signalNewChannel(const std::shared_ptr<ChannelObject> &);
    void signalNewMessage(const std::shared_ptr<MessageObject> &);

    void signalCommunicationStarted();

public slots:

private:
    void SubscribeBasicTopics();

    void SubscriberCallback(const TopicMessage& message);

    void AddUserCallback(const std::string& payload);
    void UpdateUserCallback(const std::string& payload);

    void AddChannelCallback(const std::string& payload);
    void UpdateChannelCallback(const std::string& payload);

    void ChatCallback(const std::string& topic, const std::string& payload);

    void UnknownCallback(const std::string& topic, const std::string& payload);

    std::shared_ptr<ViewsController> GetViewsController();

    std::weak_ptr<LogicController> logic_controller_;

    Subscriber sub_;
    Publisher pub_;
    ClientSocket client_;
    ChatState state_;
    std::mutex lock_;
};
