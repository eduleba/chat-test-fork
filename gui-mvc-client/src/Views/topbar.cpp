#include "topbar.h"

TopBar::TopBar()
    : user_(nullptr)
    , channel_(nullptr)
{

}

QObject *TopBar::GetUser() const
{
    return user_.get();
}

QObject *TopBar::GetChannel() const
{
    return channel_.get();
}

void TopBar::SetUser(std::shared_ptr<UserObject> user)
{
    user_ = user;
    emit signalUserChanged(GetUser());
}

void TopBar::SetChannel(std::shared_ptr<ChannelObject> channel)
{
    channel_ = channel;
    emit signalChannelChanged(GetChannel());
}
